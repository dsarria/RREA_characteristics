clear all
close all
clc



%% parameters

c_light =  299792458      ;
m_elec  =  9.10938356e-31 ;
q_elec  =  1.60217662e-19 ;


nb_ini_elec = 200 ;

fields = [6:2:30]*1e5 ;

record_times = [14.,  26.,  39.,  51.,  64.,  78.,  90.,  102.,  116.,  124.,  135., ... % 11
         144.,  153.,  164., 179.,  199.,  215.,  233.,  262.,   277.,  290.,  312., ... % 11
         351.,  406.,  464.,  479., 522.,  599.,  719.,  838.,  958., 1078.] ; % 10

record_distances = [4.,  7.,  10.,   14.,  17.,   21.,   24.,  27.,  31.,  33.,  36., ...
    38., 41.,  44.,  48.,  53.,  57.,  62.,  70.,  74.,  77.,  83., ...
    94., 108., 124.,  128.,  139.,  160.,  192.,  224.,  256.,  288.]   ;


% codenames_list = {'GEANT4O4','GEANT4O1'};
codenames_list = {'GEANT4O4'};

%% creating MATLAB data structure from text files

% /home/sarria/Desktop/HEAP/fused_RREA
% folder = '/scratch/output/fused_RREA/';
folder = '/home/sarria/Desktop/HEAP/fused_RREA/';


listing = dir(folder);

filenames = {};

for i=3:length(listing)
    filename = listing(i).name;
    filenames{i-2} = char(filename);
end


    
for codename = codenames_list
    for field = fields
        for filename = filenames
            
            full_file_path = [folder filename{1}];
            
            field_string = get_field_string(field);
            

            if (  contains(full_file_path, field_string) && contains(full_file_path, codename) ) 
                
                if exist(full_file_path, 'file') == 2
                
                    data_temp = importdata(full_file_path);
                    
                  type = data_temp(:,1);
                    xx = data_temp(:,2);
                    yy = data_temp(:,3);
                    zz = data_temp(:,4);
                    px = data_temp(:,5);
                    py = data_temp(:,6);
                    pz = data_temp(:,7);
                  time = data_temp(:,8);
%                 ener_e = data_temp(:,9); % keV
%                 ener_g = data_temp(:,9); % keV
                  
                  
                    % calculation energies from momentums
                    
                    pp = sqrt(px.^2 + py.^2 + pz.^2);
                    
                        % electrons or positrons
                        
                        mc2 = m_elec*c_light^2;
                        
                        ener_e = sqrt( (pp.*c_light).^2 + (mc2)^2 ) - mc2 ;
                        ener_g = pp.*c_light;
                        
                        % joules to keV
                        ener_e = ener_e./q_elec./1000;
                        ener_g = ener_g./q_elec./1000;

                    
                    % writing inside structure
                    
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).electron.energy = ener_e(type==-1); % keV
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).positron.energy = ener_e(type==1);
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).photon.energy   = ener_g(type==0);
                    
                    
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).electron.z = zz(type==-1); % meters
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).positron.z = zz(type==1);
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).photon.z   = zz(type==0);
                    
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).electron.y = yy(type==-1); % meters
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).positron.y = yy(type==1);
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).photon.y   = yy(type==0);
%                     
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).electron.x = xx(type==-1); % meters
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).positron.x = xx(type==1);
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).photon.x   = xx(type==0);
                    
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).electron.time = time(type==-1)*1e9; % ns
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).positron.time = time(type==1) *1e9; % ns
                    HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).photon.time   = time(type==0) *1e9; % ns
                    
                    

                    
                end
                
            end
            
        end
    end
end 


% additional information 

HEAP_data_RREA.stop_times = [1078,  522, 351, 277,  233,  199,  179,   164,  153,  144,  135, 130,   124] ;

HEAP_data_RREA.record_times=record_times;

HEAP_data_RREA.record_distances=record_distances;

HEAP_data_RREA.field_list=fields;

HEAP_data_RREA.ini_elec_nb=nb_ini_elec;

HEAP_data_RREA.codenames_list=codenames_list;




function field_string = get_field_string(field)
%% adds zeros at the begining of a string so that it has 8 characters in total
    field_string = num2str( round(field) );

    nb_to_add = 8 - length(field_string);

    if (nb_to_add>=0)
        for ii=1:nb_to_add
            field_string = ['0' field_string] ;
        end
    end

end
