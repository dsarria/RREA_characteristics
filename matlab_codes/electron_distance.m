close all
clear all
filesO4_path = '/export/scratch2/diniz/HEAP2_data/GEANT4O4_0';
filesgr_path = '/export/scratch2/rutjes/grrr_field_';
grfields = {'0000.txt','0001.txt','0002.txt','0003.txt','0004.txt','0005.txt','0006.txt'};
filesRE_path = '/export/scratch2/diniz/HEAP2_data/REAM_00';

REfields = {'600000.txt', '800000.txt', '1000000.txt', '1200000.txt','1400000.txt','1600000.txt','1800000.txt','2000000.txt','2200000.txt','2400000.txt','2600000.txt'};
O4fields = {'0600000.txt', '0800000.txt', '1000000.txt', '1200000.txt','1400000.txt','1600000.txt','1800000.txt','2000000.txt','2200000.txt','2400000.txt'};


m = matfile('/export/scratch2/diniz/HEAP2_data/HEAP_data_RREA_2.mat');
loaded = m.HEAP_data_RREA;
data = loaded.GEANT4O1;
fields = fieldnames(data);




ftel = @(a,x) (1/a).*exp(-x/a); % non-linear fit model for electrons
opts = statset('TolFun',1e-30);

j = [31 26 22 19 17 15 14 13 12 11 10 9 8]; % indices for times just before stop time
E_me = zeros(numel(fields),3); %array for fitted mean energy at all recorded distances and all electric fields
std_e = E_me; % array for the standard error on the fit
rel_err = E_me; % comparison for big errors : std_e/E_me
times = loaded.record_times(j+1); % the 12th element of stop_times there is no record_time in that time, so took other time instead
stop_distances = zeros(numel(loaded.stop_times),1);





%% parameters

c_light =  299792458      ;
m_elec  =  9.10938356e-31 ;
q_elec  =  1.60217662e-19 ;
bins=logspace(log10(10),log10(10^5),150);







for a = 1:numel(times)
    index = logical( loaded.record_times == times(a));
    temp = loaded.record_distances(index);
    stop_distances(a) = temp;
end





%---------------------------GEANT4; FIRST O4 AND THEN O1 --------------------------------------


for i = 1:numel(O4fields)
    file = strcat(filesO4_path,O4fields{i});

                  data_temp = importdata(file);
                    
                  type = data_temp(:,1);
                  zz = data_temp(:,4);
                  time = data_temp(:,8);
                  ener = data_temp(:,9);
                  
                  elecener = ener(type == -1);
                  elecdist = zz(type == -1);
                  
                  ide = logical(elecdist == stop_distances(i));

if sum(ide)>0


 sum(ide);


 [ne,edges]  = histcounts(elecener(ide),bins);

difs = diff(edges);
ne=ne./difs;
ce = sum(ide);
ne = ne/ce;
centers = (edges(1:end-1) + edges(2:end))/2.0;

fel = fitnlm(centers',ne',ftel,[5000],'Options',opts);
E_me(i,1) = fel.Coefficients.Estimate;
std_e(i,1) = fel.Coefficients.SE;
rel_err(i,1) = std_e(i,1)/E_me(i,1);
y = @(x) (1/E_me(i,1))*exp(-x/E_me(i,1));
fiel = num2str(loaded.field_list(i)*10^-6);



dist = num2str(stop_distances(i));

leg = strcat('E:', fiel,'[Mv/m]',',z:',dist,'[m]');


h(1) =  figure(1);
  loglog(centers,ne,'DisplayName',leg)
  legend('show','Location','SouthWest')
 
  hold on
 
 set(gca,'XScale','log')
  set(gca,'YScale','log')
 xlabel('Energy (keV)')
 ylabel('Counts per bin (keV-1)')

 title('GEANT4O4');
grid on

end
end



for i = 1:numel(fields)
 elecener = data.(fields{i}).electron.energy;
 elecdist = data.(fields{i}).electron.z;


ide = logical(elecdist == stop_distances(i));

if sum(ide)>0


 sum(ide);

[ne,edges]  = histcounts(elecener(ide),bins);

difs = diff(edges);
ne=ne./difs;
ce = sum(ide);
ne = ne/ce;
centers = (edges(1:end-1) + edges(2:end))/2.0;

fel = fitnlm(centers',ne',ftel,[5000],'Options',opts);
E_me(i,2) = fel.Coefficients.Estimate;
std_e(i,2) = fel.Coefficients.SE;
rel_err(i,2) = std_e(i,1)/E_me(i,1);
y = @(x) (1/E_me(i,2))*exp(-x/E_me(i,2));
fiel = num2str(loaded.field_list(i)*10^-6);



dist = num2str(stop_distances(i));

leg = strcat('E:', fiel,'[Mv/m]',',z:',dist,'[m]');


h(2) =  figure(2);
  loglog(centers,ne,'DisplayName',leg)
  legend('show','Location','SouthWest')
 
  hold on

 
% set(gca,'XScale','log')
% set(gca,'YScale','log')
 xlabel('Energy (keV)')
 ylabel('Counts per bin (keV-1)')

 title('GEANT4O1');
grid on

end
end






%---------------------------------------------------GRRR-----------------------------------



for i = 1:numel(grfields)
	
	
	
	file = strcat(filesgr_path,grfields{i});
		fid = fopen(file);
		C = textscan(fid,'%f %f %f %f %f %f %f %f','CommentStyle','#');
                  data_temp = [C{:}];
                  type = data_temp(:,1);
                  zz = data_temp(:,4);
                  px = data_temp(:,5);
                  py = data_temp(:,6);
                  pz = data_temp(:,7);

                  time = data_temp(:,8);
                    % calculation energies from momentums

                       pp = sqrt(px.^2 + py.^2 + pz.^2);
                        % electrons or positrons

                        mc2 = m_elec*c_light^2;

                       ener_e = sqrt( (pp.*c_light).^2 + (mc2)^2 ) - mc2 ;
                       ener_g = pp.*c_light;
                       % joules to keV
                       ener_e = ener_e./q_elec./1000;
                       ener_g = ener_g./q_elec./1000;


                  elecener = ener_e(type == -1);
                  elecdist = zz(type == -1);

ide = logical(elecdist == stop_distances(i));

if sum(ide)>0


 sum(ide);


 [ne,edges]  = histcounts(elecener(ide),bins);

 difs = diff(edges);
 ne=ne./difs;
 ce = sum(ide);
 ne = ne/ce;
 centers = (edges(1:end-1) + edges(2:end))/2.0;

 fel = fitnlm(centers',ne',ftel,[5000],'Options',opts);
 E_me(i,3) = fel.Coefficients.Estimate;
 std_e(i,3) = fel.Coefficients.SE;
 rel_err(i,3) = std_e(i,3)/E_me(i,3);
 y = @(x) (1/E_me(i,3))*exp(-x/E_me(i,3));
 fiel = num2str(loaded.field_list(i)*10^-6);



 dist = num2str(stop_distances(i));

 leg = strcat('E:', fiel,'[Mv/m]',',z:',dist,'[m]');

h(3) =  figure(3);
  loglog(centers,ne,'DisplayName',leg)
  legend('show','Location','SouthWest')
 
  hold on
 
%  set(gca,'XScale','log')
%  set(gca,'YScale','log')

 xlabel('Energy (keV)')
 ylabel('Counts per bin (keV-1)')
 title('GRRR');
grid on
end
end



%-------------------------------------REAM----------------------------------------------------------------



for i = 1:numel(REfields)



       file = strcat(filesRE_path,REfields{i});
                fid = fopen(file);
                C = textscan(fid,'%f %f %f %f %f %f %f %f','HeaderLines',2);
                  data_temp = [C{:}];


                  type = data_temp(:,1);
                  zz = data_temp(:,4);
                  px = data_temp(:,5);
                  py = data_temp(:,6);
                  pz = data_temp(:,7);

                  time = data_temp(:,8);
                    % calculation energies from momentums

                       pp = sqrt(px.^2 + py.^2 + pz.^2);
                        % electrons or positrons

                        mc2 = m_elec*c_light^2;

                       ener_e = sqrt( (pp.*c_light).^2 + (mc2)^2 ) - mc2 ;
                       ener_g = pp.*c_light;
                       % joules to keV
                       ener_e = ener_e./q_elec./1000;
                       ener_g = ener_g./q_elec./1000;


                  elecener = ener_e(type == -1);
                  elecdist = zz(type == -1);

ide = logical(elecdist == stop_distances(i));

if sum(ide)>0


 sum(ide);


 [ne,edges]  = histcounts(elecener(ide),bins);

 difs = diff(edges);
 ne=ne./difs;
 ce = sum(ide);
 ne = ne/ce;
 centers = (edges(1:end-1) + edges(2:end))/2.0;

 fel = fitnlm(centers',ne',ftel,[5000],'Options',opts);
 E_me(i,4) = fel.Coefficients.Estimate;
 std_e(i,4) = fel.Coefficients.SE;
 rel_err(i,4) = std_e(i,4)/E_me(i,4);
y = @(x) (1/E_me(i,4))*exp(-x/E_me(i,4));
 fiel = num2str(loaded.field_list(i)*10^-6);



 dist = num2str(stop_distances(i));

 leg = strcat('E:', fiel,'[Mv/m]',',z:',dist,'[m]');

h(4) =  figure(4);
  loglog(centers,ne,'DisplayName',leg)
  legend('show','Location','SouthWest')

  hold on

 xlabel('Energy (keV)')
 ylabel('Counts per bin (keV-1)')
 title('REAM');
grid on
end
end


ider = rel_err >= 1.0; % locate big std_e;
val_ider = rel_err(ider); % Values of big errors;

%----------------------------------Including data from Skeltveld 14 ---------------------------------------

% [x,y] -> electric field kV/m energy keV


LBE = importdata('/export/scratch2/diniz/HEAP2_data/LBE_sk14.txt');
LHEP = importdata('/export/scratch2/diniz/HEAP2_data/LHEP_sk14.txt');
DWYER = importdata('/export/scratch2/diniz/HEAP2_data/DWYER12_sk14.txt');


%kV/m to V/m

LBE(:,1) = LBE(:,1)*1000.0;
LHEP(:,1) = LHEP(:,1)*1000.0;
DWYER(:,1) = DWYER(:,1)*1000.0;






%-----------------------------------E_vs_field---------------------------------------------------------------

h(5) = figure(5);
     
    
      plot(loaded.field_list(1:numel(O4fields)),E_me(1:numel(O4fields),1),'DisplayName','GEANT4O4');
      hold on
      plot(loaded.field_list,E_me(:,2),'DisplayName','GEANT4O1');
      plot(loaded.field_list(1:numel(grfields)),E_me(1:numel(grfields),3),'DisplayName','GRRR');
      plot(loaded.field_list(1:numel(REfields)),E_me(1:numel(REfields),4),'DisplayName','REAM');
      plot(LBE(:,1),LBE(:,2),'DisplayName','LBE');
      plot(LHEP(:,1),LHEP(:,2),'DisplayName','LHEP');
      plot(DWYER(:,1),DWYER(:,2),'DisplayName','DWYER');
      xlabel('Electric Field (V/M)');
    ylabel('Mean energy (keV)');

   legend('show','Location','south');

   
   
%------------------------------saving---------------------------------------------------------------------   
for f=1:5

figname = sprintf('elec_dist_fit%02d', f);
%fig = sprintf('-f%d',f);
%print(fig,figname,'-dpdf')
savefig(h(f),figname);
end

