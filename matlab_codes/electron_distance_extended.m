%if 1 == 0

close all
clear all



filesO1_path = '/export/scratch2/diniz/HEAP2_data/GEANT4O1_0';
filesO4_path = '/export/scratch2/diniz/HEAP2_data/GEANT4O4_0';
filesgr_path = '/export/scratch2/rutjes/grrr_field_';
filesRE_path = '/export/scratch2/diniz/HEAP2_data/REAM_00';


grfields = {'0000.txt','0001.txt','0002.txt','0003.txt','0004.txt','0005.txt','0006.txt'};
O4fields = {'0600000.txt', '0800000.txt', '1000000.txt', '1200000.txt','1400000.txt','1600000.txt','1800000.txt','2000000.txt','2200000.txt','2400000.txt','2600000.txt','2800000.txt','3000000.txt'};
O1fields = {'0600000.txt', '0800000.txt', '1000000.txt', '1200000.txt','1400000.txt','1600000.txt','1800000.txt','2000000.txt','2200000.txt','2400000.txt','2600000.txt','2800000.txt','3000000.txt'};

REfields = {'600000.txt', '800000.txt', '1000000.txt', '1200000.txt','1400000.txt','1600000.txt','1800000.txt','2000000.txt','2200000.txt','2400000.txt','2600000.txt','2800000.txt','3000000.txt'};

m = matfile('/export/scratch2/diniz/HEAP2_data/HEAP_data_RREA_2.mat');
loaded = m.HEAP_data_RREA;
data = loaded.GEANT4O1;
fields = fieldnames(data);




ftel = @(a,x) (1/a).*exp(-x/a); % non-linear fit model for electrons
opts = statset('TolFun',1e-30);

j = [31 26 22 19 17 15 14 13 12 11 10 9 8]; % indices for times just before stop time
E_me = zeros(numel(fields),4); %array for fitted mean energy at all recorded distances and all electric fields
std_e = E_me; % array for the standard error on the fit
rel_err = E_me; % comparison for big errors : std_e/E_me
times = loaded.record_times(j+1); % the 12th element of stop_times there is no record_time in that time, so took other time instead
stop_distances = zeros(numel(loaded.stop_times),1);
n_counts = E_me;
errbar = E_me;



%% parameters

c_light =  299792458      ;
m_elec  =  9.10938356e-31 ;
q_elec  =  1.60217662e-19 ;
bins=logspace(log10(10),log10(10^5),150);







for a = 1:numel(times)
    index = logical( loaded.record_times == times(a));
    temp = loaded.record_distances(index);
    stop_distances(a) = temp;
end


if 1== 0


%---------------------------GEANT4; FIRST O4 AND THEN O1 --------------------------------------


for i =1:numel(O4fields)
    file = strcat(filesO4_path,O4fields{i});
if i>8
       %           data_temp = importdata(file);
                fid = fopen(file);
	ne = zeros(1,length(bins)-1);
	counter = 0;
	while (feof(fid) == 0);
		counter = counter +1; 
%		C = textscan(fid,'%f %f %f %f %f %f %f %f %f %f',1,'CommentStyle','#');
%		C = dlmread(file,' ',[counter 0 counter+1 10])
		data_temp = fscanf(fid,'%f %f %f %f %f %f %f %f %f %f',[1 10]);
%		data_temp = [C(:)];
           if(isempty(data_temp) == 0); 
                  type = data_temp(:,2);
                  zz = data_temp(:,5);
                  time = data_temp(:,9);
                  ener = data_temp(:,10);
		                  
                  elecener = ener(type == -1);
                  elecdist = zz(type == -1);
                  
                  ide = logical(elecdist == stop_distances(i));

		if sum(ide)>0

			[net,edges]  = histcounts(elecener(ide),bins);
			ne(:) = ne(:) + net(:);
		end
	   end
	end


i
else
       %           data_temp = importdata(file);
                fid = fopen(file);

                C = textscan(fid,'%f %f %f %f %f %f %f %f %f %f','CommentStyle','#');
                  data_temp = [C{:}];

                  type = data_temp(:,2);
                  zz = data_temp(:,5);
                  time = data_temp(:,9);
                  ener = data_temp(:,10);

                  elecener = ener(type == -1);
                  elecdist = zz(type == -1);

                  ide = logical(elecdist == stop_distances(i));

		if sum(ide)>0



		[ne,edges]  = histcounts(elecener(ide),bins);
		end
i
end
ce = sum(ne);
fclose(fid)
difs = diff(edges);
ne=ne./difs;
%ce = sum(ide);
ne = ne/ce;
centers = (edges(1:end-1) + edges(2:end))/2.0;

fel = fitnlm(centers',ne',ftel,[5000],'Options',opts);
E_me(i,1) = fel.Coefficients.Estimate
std_e(i,1) = fel.Coefficients.SE;
rel_err(i,1) = std_e(i,1)/E_me(i,1);
%y = @(x) (1/E_me(i,1))*exp(-x/E_me(i,1));
%fiel = num2str(loaded.field_list(i)*10^-6);
n_counts(i,1) = ce;
clearvars type zz time data_temp C elecener elecdist ide ne;
%dist = num2str(stop_distances(i));
%
%leg = strcat('E:', fiel,'[Mv/m]',',z:',dist,'[m]');
%
%
%h(1) =  figure(1);
%  loglog(centers,ne,'DisplayName',leg)
%  legend('show','Location','SouthWest')
% 
%  hold on
% 
% set(gca,'XScale','log')
%  set(gca,'YScale','log')
% xlabel('Energy (keV)')
% ylabel('Counts per bin (keV-1)')
%
% title('GEANT4O4');
%grid on
end


end

%if 1 == 0

for i = 1:numel(O1fields)

        file = strcat(filesO1_path,O1fields{i});
                fid = fopen(file);
                C = textscan(fid,'%f %f %f %f %f %f %f %f %f %f','CommentStyle','#');
                  data_temp = [C{:}];

                  type = data_temp(:,2);
                  zz = data_temp(:,5);
                  time = data_temp(:,9);
                  ener = data_temp(:,10);

                  elecener = ener(type == -1);
                  elecdist = zz(type == -1);

                  ide = logical(elecdist == stop_distances(i));

       







ide = logical(elecdist == stop_distances(i));

if sum(ide)>0


 sum(ide);

[ne,edges]  = histcounts(elecener(ide),bins);

difs = diff(edges);
ne=ne./difs;
ce = sum(ide);
ne = ne/ce;
centers = (edges(1:end-1) + edges(2:end))/2.0;

fel = fitnlm(centers',ne',ftel,[5000],'Options',opts);
E_me(i,2) = fel.Coefficients.Estimate;
std_e(i,2) = fel.Coefficients.SE;
rel_err(i,2) = std_e(i,1)/E_me(i,1);
%y = @(x) (1/E_me(i,2))*exp(-x/E_me(i,2));
%fiel = num2str(loaded.field_list(i)*10^-6);
n_counts(i,2) = ce;
%
%
%dist = num2str(stop_distances(i));
%
%leg = strcat('E:', fiel,'[Mv/m]',',z:',dist,'[m]');
%
%
%h(2) =  figure(2);
%  loglog(centers,ne,'DisplayName',leg)
%  legend('show','Location','SouthWest')
% 
%  hold on
%
% 
% set(gca,'XScale','log')
% set(gca,'YScale','log')
% xlabel('Energy (keV)')
% ylabel('Counts per bin (keV-1)')
%
% title('GEANT4O1');
%grid on

end
end

%end




%---------------------------------------------------GRRR-----------------------------------

if 1 == 0

for i = 1:numel(grfields)
	
	
	
	file = strcat(filesgr_path,grfields{i});
		fid = fopen(file);
		C = textscan(fid,'%f %f %f %f %f %f %f %f','CommentStyle','#');
                  data_temp = [C{:}];
                  type = data_temp(:,1);
                  zz = data_temp(:,4);
                  px = data_temp(:,5);
                  py = data_temp(:,6);
                  pz = data_temp(:,7);

                  time = data_temp(:,8);
                    % calculation energies from momentums

                       pp = sqrt(px.^2 + py.^2 + pz.^2);
                        % electrons or positrons

                        mc2 = m_elec*c_light^2;

                       ener_e = sqrt( (pp.*c_light).^2 + (mc2)^2 ) - mc2 ;
                       ener_g = pp.*c_light;
                       % joules to keV
                       ener_e = ener_e./q_elec./1000;
                       ener_g = ener_g./q_elec./1000;


                  elecener = ener_e(type == -1);
                  elecdist = zz(type == -1);

ide = logical(elecdist == stop_distances(i));

if sum(ide)>0


 sum(ide);


 [ne,edges]  = histcounts(elecener(ide),bins);

 difs = diff(edges);
 ne=ne./difs;
 ce = sum(ide);
 ne = ne/ce;
 centers = (edges(1:end-1) + edges(2:end))/2.0;

 fel = fitnlm(centers',ne',ftel,[5000],'Options',opts);
 E_me(i,3) = fel.Coefficients.Estimate;
 std_e(i,3) = fel.Coefficients.SE;
 rel_err(i,3) = std_e(i,3)/E_me(i,3);
 y = @(x) (1/E_me(i,3))*exp(-x/E_me(i,3));
 fiel = num2str(loaded.field_list(i)*10^-6);
n_counts(i,3) = ce;


 dist = num2str(stop_distances(i));

 leg = strcat('E:', fiel,'[Mv/m]',',z:',dist,'[m]');

h(3) =  figure(3);
  loglog(centers,ne,'DisplayName',leg)
  legend('show','Location','SouthWest')
 
  hold on
 
%  set(gca,'XScale','log')
%  set(gca,'YScale','log')

 xlabel('Energy (keV)')
 ylabel('Counts per bin (keV-1)')
 title('GRRR');
grid on
end
end


ider = rel_err >= 1.0; % locate big std_e;
val_ider = rel_err(ider); % Values of big errors;


%-------------------------------------REAM----------------------------------------------------------------



for i = 1:numel(REfields)-2



       file = strcat(filesRE_path,REfields{i});
                fid = fopen(file);
                C = textscan(fid,'%f %f %f %f %f %f %f %f','HeaderLines',2);
                  data_temp = [C{:}];


                  type = data_temp(:,1);
                  zz = data_temp(:,4);
                  px = data_temp(:,5);
                  py = data_temp(:,6);
                  pz = data_temp(:,7);

                  time = data_temp(:,8);
                    % calculation energies from momentums

                       pp = sqrt(px.^2 + py.^2 + pz.^2);
                        % electrons or positrons

                        mc2 = m_elec*c_light^2;

                       ener_e = sqrt( (pp.*c_light).^2 + (mc2)^2 ) - mc2 ;
                       ener_g = pp.*c_light;
                       % joules to keV
                       ener_e = ener_e./q_elec./1000;
                       ener_g = ener_g./q_elec./1000;


                  elecener = ener_e(type == -1);
                  elecdist = zz(type == -1);

ide = logical(elecdist == stop_distances(i));

if sum(ide)>0


 sum(ide);


 [ne,edges]  = histcounts(elecener(ide),bins);

 difs = diff(edges);
 ne=ne./difs;
 ce = sum(ide);
 ne = ne/ce;
 centers = (edges(1:end-1) + edges(2:end))/2.0;

 fel = fitnlm(centers',ne',ftel,[5000],'Options',opts);
 E_me(i,4) = fel.Coefficients.Estimate;
 std_e(i,4) = fel.Coefficients.SE;
 rel_err(i,4) = std_e(i,4)/E_me(i,4);
 y = @(x) (1/E_me(i,4))*exp(-x/E_me(i,4));
 fiel = num2str(loaded.field_list(i)*10^-6);
n_counts(i,4) = ce;


 dist = num2str(stop_distances(i));

 leg = strcat('E:', fiel,'[Mv/m]',',z:',dist,'[m]');

h(4) =  figure(4);
  loglog(centers,ne,'DisplayName',leg)
  legend('show','Location','SouthWest')
 
  hold on

 xlabel('Energy (keV)')
 ylabel('Counts per bin (keV-1)')
 title('REAM');
grid on
end
end


ider = rel_err >= 1.0; % locate big std_e;
val_ider = rel_err(ider); % Values of big errors;




%----------------------------------Including data from Skeltveld 14 ----------NO-MORE-----------------------------

% [x,y] -> electric field kV/m energy keV


%LBE = importdata('/export/scratch2/diniz/HEAP2_data/LBE_sk14.txt');
%LHEP = importdata('/export/scratch2/diniz/HEAP2_data/LHEP_sk14.txt');
%DWYER = importdata('/export/scratch2/diniz/HEAP2_data/DWYER12_sk14.txt');


%kV/m to V/m

%LBE(:,1) = LBE(:,1)*1000.0;
%LHEP(:,1) = LHEP(:,1)*1000.0;
%DWYER(:,1) = DWYER(:,1)*1000.0;



end
close all
%---------------------------------FIT E_VS_FIELD----------------------------------------------
ene_f = @(a,b,c,x) (c_light./(a.*(x.^b) + c)).*(x - 2.8E5)./1000; % non-linear fit model for mean energy
beta0 = [55.0, 1.0 -1e4];
%opts = fitoptions('StartPoints',beta0);
LOW =  [0.0, 0.0, -1e7];
UP = [70.0, 2.0, 0];
%opts.Lower = LOW;
%opts.Upper = UP;
%en_O4 =  fit(loaded.field_list(1:numel(O4fields))',E_me(1:numel(O4fields),1),ene_f,'StartPoint',beta0,'Lower',LOW,'Upper',UP);
en_O1 =  fit(loaded.field_list(1:numel(O1fields))',E_me(1:numel(O1fields),2),ene_f,'StartPoint',beta0,'Lower',LOW,'Upper',UP);
%en_GR =  fit(loaded.field_list(1:numel(grfields))',E_me(1:numel(grfields),3),ene_f,'StartPoint',beta0,'Lower',LOW,'Upper',UP);
%en_RE =  fit(loaded.field_list(2:numel(REfields)-2)',E_me(2:numel(REfields)-2,4),ene_f,'StartPoint',beta0,'Lower',LOW,'Upper',UP);

CoeffsO1 = coeffvalues(en_O1);
%CoeffsO4 = coeffvalues(en_O4);
%CoeffsGR = coeffvalues(en_GR);
%CoeffsRE = coeffvalues(en_RE);



%O1_1 = en_O1.Coefficients.Estimate(1);
%O1_2 = en_O1.Coefficients.Estimate(2);
%O1_3 = en_O1.Coefficients.Estimate(3);
%O1_4 = en_O1.Coefficients.Estimate(4);


%O4_1 = en_O4.Coefficients.Estimate(1);
%O4_2 = en_O4.Coefficients.Estimate(2);
%O4_3 = en_O4.Coefficients.Estimate(3);
%O4_4 = en_O4.Coefficients.Estimate(4);


%GR_1 = en_GR.Coefficients.Estimate(1);
%GR_2 = en_GR.Coefficients.Estimate(2);
%GR_3 = en_GR.Coefficients.Estimate(3);
%GR_4 = en_GR.Coefficients.Estimate(4);


%RE_1 = en_RE.Coefficients.Estimate(1);
%RE_2 = en_RE.Coefficients.Estimate(2);
%RE_3 = en_RE.Coefficients.Estimate(3);
%RE_4 = en_RE.Coefficients.Estimate(4);






%-----------------------------------E_vs_field---------------------------------------------------------------
errbar = zeros(numel(fields),4);
for i=1:13
	for j=1:4
if n_counts(i,j)>0
errbar(i,j) = (2.*std_e(i,j));
end
end
end
electricfield = linspace(loaded.field_list(1),loaded.field_list(end),100);

h(5) = figure(5);
     
%      errorbar(loaded.field_list(1:numel(O4fields)),E_me(1:numel(O4fields),1),errbar(1:numel(O4fields),1),'DisplayName','GEANT4O4');
%      hold on
%      plot(electricfield,ene_f(CoeffsO4(1),CoeffsO4(2),CoeffsO4(3),electricfield),'DisplayName','O4 FIT');

      errorbar(loaded.field_list,E_me(:,2),errbar(:,2),'DisplayName','GEANT4O1');
      hold on
      plot(electricfield,ene_f(CoeffsO1(1),CoeffsO1(2),CoeffsO1(3),electricfield),'DisplayName','O1 FIT');
%      plot(electricfield,ene_f(CoeffsGR(1),CoeffsGR(2),CoeffsGR(3),electricfield),'DisplayName','GR FIT');
%      errorbar(loaded.field_list(1:numel(grfields)),E_me(1:numel(grfields),3),errbar(1:numel(grfields),3),'DisplayName','GRRR');
%      plot(electricfield,ene_f(CoeffsRE(1),CoeffsRE(2),CoeffsRE(3),electricfield),'DisplayName','RE FIT');
%      errorbar(loaded.field_list(2:numel(REfields)-2),E_me(2:numel(REfields)-2,4),errbar(2:numel(REfields)-2,4),'DisplayName','REAM');
      xlabel('Electric Field (V/M)');
    ylabel('Mean energy (keV)');

   legend('show','Location','southeast');

   
   
%------------------------------saving---------------------------------------------------------------------   
for f = 5
%for f= 1:5

figname = sprintf('elec_distO1_new_max_step_fit%02d', f);
%fig = sprintf('-f%d',f);
%print(fig,figname,'-dpdf')
savefig(h(f),figname);
end

