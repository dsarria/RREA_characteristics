clear all
close all
clc

model_list = ["O1","O4","PEN","SS2","LEP","GS","LBE","WVI"];
% ener_thres_list= [990., 250., 100.]  ;

% model_list = ["O1","O4"];
ener_thres_list= [990., 250., 100.]  ;

Distance_to_Tstop = 62; % meters

%% reading and gathering the data
folder = '/scratch/output_RREA_CHAR/output_RREA_100keV/14e5V_m_different_physics_and_thres/';

listing = dir(folder);

filenames = {};

for i=3:length(listing)
    filename = listing(i).name;
    filenames{i-2} = char(filename);
end

data_RREA.field = 14e5; % V/m
data_RREA.field_Unit = "V/m";

for model = model_list
    for ener_thres = ener_thres_list
        for filename = filenames
            clear data_temp data_temp2
            
            
            full_file_path = [folder filename{1}];
            ener_thres_string = num2str(ener_thres);
            
            ener_thres_string_full = ['_' num2str(ener_thres) '.txt'];
            
            if (contains(full_file_path, ener_thres_string) && contains(full_file_path, ['_GEANT4' char(model) '_' ]))
                
                data_temp = importdata(full_file_path);
                data_temp2 = data_temp(   data_temp(:,5) == Distance_to_Tstop,:);
                
                % creating fields if does not exist
                if ~( isfield(data_RREA,(char(model))))
                    data_RREA.(char(model)) = [];
                end
                
                if ~( isfield(data_RREA.(char(model)),['thres_' ener_thres_string]))
                    data_RREA.(char(model)).(['thres_' ener_thres_string]) = [];
                end
                
                if ~( isfield(data_RREA.(char(model)).(['thres_' ener_thres_string]),'data'))
                    data_RREA.(char(model)).(['thres_' ener_thres_string]).data = [];
                end
                %
                
                % data import and concatenation
                data_RREA.(char(model)).(['thres_' ener_thres_string]).data = [ data_RREA.(char(model)).(['thres_' ener_thres_string]).data; data_temp2];
                
                
            end
        end
    end
end



%%
clear data_temp data_temp2 name mean_ener mean_ener2
close all

friction_data = importdata('friction_curve');
friction = friction_data(:,2) .* 10; % to V/m
energy = friction_data(:,1) ./ 1000.; % to keV

[XI] = findX(energy,friction,14.e5)



jj=0;

for model = model_list
    for ener_thres = ener_thres_list
        
        clear data_temp;
        
        full_file_path = [folder filename{1}];
        ener_thres_string = num2str(ener_thres);
        
        data_temp = data_RREA.(char(model)).(['thres_' ener_thres_string]).data ;
        energies = data_temp(:,10);
        
        data_RREA.(char(model)).(['thres_' ener_thres_string]).mean_ener = mean(energies(energies>26.));
        
        
        
        jj = jj+1;
        name{jj} = ['{' char(model) ' thres ' ener_thres_string '}'] ;
        mean_ener(jj) = expfit(energies);
        mean_ener1(jj) = mean(energies);
        mean_ener2(jj) = mean(energies(energies>26.));
        mean_ener3(jj) = expfit(energies(energies>7000.));
        
        % color and tick style
        if (model=="O1")
            color = 'r';
        elseif (model=="O4")
            color = 'b';
        elseif (model=="PEN")
            color = 'k';
        elseif (model=="LIV")
            color = 'g';
        elseif (model=="SS")
            color = 'm';
        elseif (model=="LEP")
            color = 'r';
        elseif (model=="GS")
            color = 'b';
        elseif (model=="LBE")
            color = 'k';
        elseif (model=="LIVPOLAR")
            color = 'g';
        elseif (model=="WVI")
            color = 'm';
        elseif (model=="SS2")
            color = 'r';
        end
        
        
        if (ener_thres == 250.)
            tickstyle = '+';
        elseif (ener_thres == 990.)
            tickstyle = '*';
        elseif (ener_thres == 100.)
            tickstyle = 'x';
        end
        %
        
        plot(jj,mean_ener(jj),'Color',color,'Marker', tickstyle, 'markers', 5 ) ;
        hold on
        plot(jj,mean_ener1(jj),'Color',color,'Marker', tickstyle, 'markers', 20 ) ;
        plot(jj,mean_ener2(jj),'Color',color,'Marker', tickstyle, 'markers', 40 ) ;
        plot(jj,mean_ener3(jj),'Color',color,'Marker', tickstyle, 'markers', 60 ) ;
        
    end
end

abs(mean_ener2-mean_ener)

mean_ener(end+1) = 7459.59;
name{end+1} = 'Dwyer (Alex paper)' ;
plot(length(mean_ener),mean_ener(end),'+r','markers',20)

mean_ener(end+1) = 7500.00;
name{end+1} = 'LBE (Alex paper)' ;
plot(length(mean_ener),mean_ener(end),'+g','markers',20)

mean_ener(end+1) = 7257.57;
name{end+1} = 'GRRR' ;
plot(length(mean_ener),mean_ener(end),'+b','markers',20)

% plot(1:length(mean_ener), mean_ener,'+','markers',20)

xticks(1:length(mean_ener))
xticklabels(name)
xtickangle(45)
grid on
ylabel('mean energy (keV) from using expfit on the energy list')
title('E-field of 14e5 V/m, record at z = 62 meter')