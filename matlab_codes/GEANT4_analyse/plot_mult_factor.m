clear all
close all
clc


%%

nb_ini = 200;

fields = [6:2:30]*1e5 ;

record_times = [14.,  26.,  39.,  51.,  64.,  78.,  90.,  102.,  116.,  124.,  135., ... % 11
    144.,  153.,  164., 179.,  199.,  215.,  233.,  262.,   277.,  290.,  312., ... % 11
    351.,  406.,  464.,  479., 522.,  599.,  719.,  838.,  958., 1078.] ; % 10

record_distances = [4.,  7.,  10.,   14.,  17.,   21.,   24.,  27.,  31.,  33.,  36., ... % 11
    38., 41.,  44.,  48.,  53.,  57.,  62.,  70.,  74.,  77.,  83., ... % 11
    94., 108., 124.,  128.,  139.,  160.,  192.,  224.,  256.,  288.]   ; % 10

stop_times = [1078,  522, 351, 277,  233,  199,  179,   164,  153,  144,  135, 130,   124] ;

corr_dist = [288,  139, 94, 74,  233,  62,  53,   48,  44,  41,  38, 36,   33] ;

data_folder = '/scratch/output_RREA_CHAR/output_RREA_100keV/output/O1_1cm_990eV/';

codenames_list = {'GEANT4O1'};

listing = dir(data_folder);

filenames = {};

for i=3:length(listing)
    filename = listing(i).name;
    filenames{i-2} = char(filename);
end

%%

for codename = codenames_list
    i_field=1;
    for field = fields
        for filename = filenames
            
            full_file_path = [data_folder filename{1}];
            
            field_string = get_field_string(field);
            
            if (  contains(full_file_path, field_string) && contains(full_file_path, codename) )
                if exist(full_file_path, 'file') == 2
                    
                    data_temp = importdata(full_file_path);
                    
                    type = data_temp(:,2);
                    data_temp = data_temp(type==-1,:);
                    
                    time = data_temp(:,9)*1e9; % s to ns
                    
                    
                    ener = data_temp(:,10);
                    
                    zz = data_temp(:,5);
                    
                    zz(time>stop_times(i_field))=[];
                    
                    jj=1;
                    kk=1;
                    
                    for dist = record_distances
                        mul_fact.('field')(i_field).('dist')(jj) = sum(zz==dist) / nb_ini;
                        jj=jj+1;
                    end
                    
                    for timee = record_times
                        mul_fact.('field')(i_field).('time')(kk) = sum(time==timee) / nb_ini;
                        kk=kk+1;
                    end
                    
                    
                    
                end
            end
        end
        i_field=i_field+1;
    end
end

close all
figure(1)
for i=1:length(mul_fact.field)
    semilogy(record_distances,mul_fact.field(i).dist)
    thelegend{i} = ['E-field : ' num2str(fields(i)/1e5) 'e5 V/m'];
    hold on
end

xlabel('Z distance (meter)')
ylabel('multiplciation factor')
legend(thelegend)
grid on

figure(2)
for i=1:length(mul_fact.field)
    semilogy(record_times,mul_fact.field(i).time)
    hold on
end

xlabel('Time (nanosecond)')
ylabel('multiplciation factor')
legend(thelegend)
grid on


%%

function field_string = get_field_string(field)
%% adds zeros at the begining of a string so that it has 8 characters in total
field_string = num2str( round(field) );

nb_to_add = 8 - length(field_string);

if (nb_to_add>=0)
    for ii=1:nb_to_add
        field_string = ['0' field_string] ;
    end
end

end
