clear all
close all
clc

ener_thresS = [100];
% models = {'O4','O1','PEN','LBE','WVI'};
% models = {'WVI','PEN'};
models = {'PEN'};
field = '1400000';

I_legend=1;

for model = models
    for ener_thres = ener_thresS
        
        folder = '/Home/siv29/dsa030/Desktop/GEANT4/HEAP/RREA_characteristics_record_at_stopTime_only/GEANT4_cluster/code/build/output/';
        filename = ['GEANT4' model{1} '_' field '_' num2str(ener_thres) '.txt'];
        
        thelegend{I_legend} = ['{' 'GEANT4' model{1} ' thres = ' num2str(ener_thres) 'eV }'];
        I_legend = I_legend+1;
        
        yy = importdata([folder filename]);
        
        energies = yy(:,10);
        type = yy(:,2);
        
        min(energies)
        
        
        energies = energies(type==-1);
        
        [counts, bins] = make_spec(energies, ener_thres/1000.);
        
        figure(1)
        histogram('BinEdges',bins,'BinCounts',counts,'DisplayStyle','stairs')
        set(gca, 'YScale', 'log')
        set(gca, 'XScale', 'log')
        grid on
        hold on
        
        
        plot_meanEner_vs_minEner(energies, ener_thres/1000., model{1})
        
    end
end

figure(1)
legend(thelegend)
title('E-field = 14e5 V/M, recorded at 233 us')

figure(2)
legend(thelegend)
title('E-field = 14e5 V/M, recorded at 233 us')

%%


function plot_meanEner_vs_minEner(energies, ener_thres_model,modelname)
eners_thres = logspace(log10(ener_thres_model),3,120) ;

for i=1:length(eners_thres)
    mean_ener(i) = mean(energies(energies>eners_thres(i)));
end
figure(2)

tick='+';

if modelname=="LBE"
    color='r';
elseif modelname=="O1"
    color='k';
elseif modelname=="O4"
    color='m';
elseif modelname=="SS"
    color='g';
elseif modelname=="WVI"
    color='b';
elseif modelname=="PEN"
    color='g';
    tick='x';
end


semilogx(eners_thres,mean_ener,'Color',color, 'Marker',tick)
hold on
grid on
xlabel('minimum energy for average calculation')
ylabel('average energy (keV)')
end



function [counts, bins] = make_spec(eners, ener_thres_model)

bins = logspace(log10(0.01),log10(100000),120);

eners(eners<ener_thres_model)=[];

[counts,bins] = histcounts(eners,bins);
counts = counts./diff(bins);

end

