close all
%clear all

filesO4_path = '/export/scratch2/diniz/HEAP2_data/GEANT4O4_0';
filesgr_path = '/export/scratch2/rutjes/grrr_field_';
grfields = {'0000.txt','0001.txt','0002.txt','0003.txt','0004.txt','0005.txt','0006.txt'};


O4fields = {'0600000.txt', '0800000.txt', '1000000.txt', '1200000.txt','1400000.txt','1600000.txt','1800000.txt','2000000.txt','2200000.txt','2400000.txt'};


m = matfile('/export/scratch2/diniz/HEAP2_data/HEAP_data_RREA_2.mat');
loaded = m.HEAP_data_RREA;
data = loaded.GEANT4O1;
O1fields = fieldnames(data);
ftel = @(a,x) a(1)*((x./a(2)).^(-a(3))).*(x < cf) + (x >= cf).*(1/a(4)).*exp(-x./a(4)); % non-linear fit model for electrons
opts = statset('TolFun',1e-30);
E_me = zeros(numel(fields),3); % array for fitted mean energy at all recorded times and all electric fields
std_e = E_me; % array for the standard error on the fit
rel_err = E_me; % comparison for big errors : std_e/E_me
j = [31 26 22 19 17 15 14 13 12 11 10 9 8]; % indices for times just before stop time
times = loaded.record_times(j); % specific time moments





%% parameters

c_light =  299792458      ;
m_elec  =  9.10938356e-31 ;
q_elec  =  1.60217662e-19 ;
bins=logspace(log10(10),log10(10^5),150);
cf = 1000;


% For Bethe friction ------------ DWYER 11
fre = @(e,x) (1./e).*exp(-x./e);  
Ion = 85.7e-3; % ionization factor for air
mc2 = (10^-3)*(m_elec*c_light^2)/q_elec; % in keV
E_b =  1e3 ; % 1 MeV  ---- DIFFERENTS E_b for different Electric fields on DWYER 11
Nre = @(e) exp(-E_b./e);
beta = @(x) sqrt(1 - (mc2./(x + mc2)).^2);
gamma = @(x) sqrt((1 -(beta(x)).^2).^(-1)); 
corr = @(x) log((mc2.^2).*(gamma(x).^2 -1).*(gamma(x) -1)./Ion) - (1 + 2./gamma(x) + 1./gamma(x).^2).*log(2) + 1./gamma(x).^2 + ((gamma(x) -1).^2)./(8.*gamma(x).^2); % factor on brackets
fint = @(e,x) Nre(e).*beta(x).*(1./x - 1./E_b).*corr(x).^-1 + fre(e,E_b).*(beta(E_b)./beta(x)).*(corr(E_b)./corr(x)); % intermediate electron function Dwyer 2011
fd = @(e,x) fint(e,x).*(x < E_b) + ( x >= E_b).*fre(e,x);  % Whole distribution Dwyer 11


%  e value is the mean energy on the exponential distribution and x is the data energy (centers)


% For power-law fit  ---------- START values ------------------------------------

a1 = 0.0001;
a2 = 1000;
a3 = 0.42;
a4 = 7000;





%---------------------------GEANT4; FIRST O4 AND THEN O1 --------------------------------------


for i = 1:numel(O4fields)
    file = strcat(filesO4_path,O4fields{i});

                  data_temp = importdata(file);

                  type = data_temp(:,1);
                  time = data_temp(:,8);
                  ener = data_temp(:,9);

                  elecener = ener(type == -1);
                  electim = time(type == -1);

                  ide = logical(electim == times(i));

if sum(ide)>0

 [ne,edges]  = histcounts(elecener(ide),bins);
centers = (edges(1:end-1) + edges(2:end))/2.0;


 difs = diff(edges);

 ne=ne./difs;
 ce = sum(ide);
 ne = ne/ce;




fel = fitnlm(centers',ne',ftel,[a1 a2 a3 a4],'Options',opts); % power law fit
b1 = fel.Coefficients.Estimate(1);
b2 = fel.Coefficients.Estimate(2);
b3 = fel.Coefficients.Estimate(3);
b4 = fel.Coefficients.Estimate(4);


E_me(i,1) = b1*(b2^b3)*(cf^(2 - b3))/(2 - b3) + b4*exp(- cf/b4)*(1 + cf/b4);
g = @(x) b1*((x./b2).^(-b3)).*(x < cf) + (x >= cf).*(1/b4).*exp(-x./b4); % non-linear fit model for electrons



spece2 = fd(7300, centers);
spece = g(centers);
figure;

field = num2str(loaded.field_list(i)*10^-6);

 loglog(centers,ne,'DisplayName','data');
 hold on

   plot(centers,spece,'DisplayName','power law');
   plot(centers, spece2,'DisplayName','Dwyer 11');
  set(gca,'XScale','log')
   set(gca,'YScale','log')
  xlabel('Energy (keV)')
  ylabel('Counts per bin (keV-1)')
tit = strcat('E =', field, ' MV/m');
 title(tit);
 grid on
 legend('show','Location','South');
end
end




for i = 1:numel(O1fields)
 elecener = data.(O1fields{i}).electron.energy;
 electim = data.(O1fields{i}).electron.time;


ide = logical(electim == times(i));

if sum(ide)>0


 sum(ide);

 [ne,edges]  = histcounts(elecener(ide),bins);
centers = (edges(1:end-1) + edges(2:end))/2.0;


 difs = diff(edges);

 ne=ne./difs;
 ce = sum(ide);
 ne = ne/ce;




fel = fitnlm(centers',ne',ftel,[a1 a2 a3 a4],'Options',opts); % power law fit
b1 = fel.Coefficients.Estimate(1);
b2 = fel.Coefficients.Estimate(2);
b3 = fel.Coefficients.Estimate(3);
b4 = fel.Coefficients.Estimate(4);


E_me(i,2) = b1*(b2^b3)*(cf^(2 - b3))/(2 - b3) + b4*exp(- cf/b4)*(1 + cf/b4);
g = @(x) b1*((x./b2).^(-b3)).*(x < cf) + (x >= cf).*(1/b4).*exp(-x./b4); % non-linear fit model for electrons


spece2 = fd(7300, centers);
spece = g(centers);
figure;

 loglog(centers,ne,'DisplayName','data');
 hold on 

   plot(centers,spece,'DisplayName','power law'); 
   plot(centers, spece2,'DisplayName','Dwyer 11');
  set(gca,'XScale','log')
   set(gca,'YScale','log')
  xlabel('Energy (keV)')
  ylabel('Counts per bin (keV-1)')
field = num2str(loaded.field_list(i)*10^-6);
  tit = strcat('E =', field, ' MV/m');
  title(tit);
 grid on
 legend('show','Location','South');
end
end






%---------------------------------------------------GRRR-----------------------------------



for i = 1:numel(grfields)



        file = strcat(filesgr_path,grfields{i});
                fid = fopen(file);
                C = textscan(fid,'%f %f %f %f %f %f %f %f','CommentStyle','#');
                  data_temp = [C{:}];
                  type = data_temp(:,1);
                  zz = data_temp(:,4);
                  px = data_temp(:,5);
                  py = data_temp(:,6);
                  pz = data_temp(:,7);

                  time = data_temp(:,8);
                    % calculation energies from momentums

                       pp = sqrt(px.^2 + py.^2 + pz.^2);
                        % electrons or positrons

                        mc2 = m_elec*c_light^2;

                       ener_e = sqrt( (pp.*c_light).^2 + (mc2)^2 ) - mc2 ;
                       ener_g = pp.*c_light;
                       % joules to keV
                       ener_e = ener_e./q_elec./1000;
                       ener_g = ener_g./q_elec./1000;


                  elecener = ener_e(type == -1);
                  electim = time(type == -1);

ide = logical(electim == times(i));



if sum(ide)>0


 sum(ide);

 [ne,edges]  = histcounts(elecener(ide),bins);
centers = (edges(1:end-1) + edges(2:end))/2.0;


 difs = diff(edges);

 ne=ne./difs;
 ce = sum(ide);
 ne = ne/ce;




fel = fitnlm(centers',ne',ftel,[a1 a2 a3 a4],'Options',opts); % power law fit
b1 = fel.Coefficients.Estimate(1);
b2 = fel.Coefficients.Estimate(2);
b3 = fel.Coefficients.Estimate(3);
b4 = fel.Coefficients.Estimate(4);


E_me(i,3) = b1*(b2^b3)*(cf^(2 - b3))/(2 - b3) + b4*exp(- cf/b4)*(1 + cf/b4);
g = @(x) b1*((x./b2).^(-b3)).*(x < cf) + (x >= cf).*(1/b4).*exp(-x./b4); % non-linear fit model for electrons




spece2 = fd(7300, centers);
spece = g(centers);
figure;

 loglog(centers,ne,'DisplayName','data');
 hold on

   plot(centers,spece,'DisplayName','power law');
   plot(centers, spece2,'DisplayName','Dwyer 11');
  set(gca,'XScale','log')
   set(gca,'YScale','log')
  xlabel('Energy (keV)')
  ylabel('Counts per bin (keV-1)')
 field = num2str(loaded.field_list(i)*10^-6); 
 tit = strcat('E =', field, ' MV/m');
 title(tit);
 grid on
 legend('show','Location','South');
end
end




% 'mean energy' vs E--------------------------------------------------------------------------



%----------------------------------Including data from Skeltveld 14 -----(space output)-----------------

% [x,y] -> electric field kV/m energy keV


LBE = importdata('/export/scratch2/diniz/HEAP2_data/LBE_sk14.txt');
LHEP = importdata('/export/scratch2/diniz/HEAP2_data/LHEP_sk14.txt');
DWYER = importdata('/export/scratch2/diniz/HEAP2_data/DWYER12_sk14.txt');


%kV/m to V/m

LBE(:,1) = LBE(:,1)*1000.0;
LHEP(:,1) = LHEP(:,1)*1000.0;
DWYER(:,1) = DWYER(:,1)*1000.0;






%-----------------------------------E_vs_field---------------------------------------------------------------

figure;


      plot(loaded.field_list(1:numel(O4fields)),E_me(1:numel(O4fields),1),'DisplayName','GEANT4O4');
      hold on
      plot(loaded.field_list,E_me(:,2),'DisplayName','GEANT4O1');
      plot(loaded.field_list(1:numel(grfields)),E_me(1:numel(grfields),3),'DisplayName','GRRR');
      plot(LBE(:,1),LBE(:,2),'DisplayName','LBE');
      plot(LHEP(:,1),LHEP(:,2),'DisplayName','LHEP');
      plot(DWYER(:,1),DWYER(:,2),'DisplayName','DWYER');
      xlabel('Electric Field (V/M)');
    ylabel('Mean energy (keV)');

   legend('show','Location','south');





%------------------------------saving---------------------------------------------------------------------
handles=findall(0,'type','figure');

for f=1:length(handles)

figname = sprintf('elec_time_fit%02d', f);
%fig = sprintf('-f%d',f);
%print(fig,figname,'-dpdf')
savefig(handles(f),figname);
end

