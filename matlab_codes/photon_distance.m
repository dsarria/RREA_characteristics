close all
%clear all
filesO4_path = '/export/scratch2/diniz/HEAP2_data/GEANT4O4_0';
filesgr_path = '/export/scratch2/rutjes/grrr_field_';
grfields = {'0000.txt','0001.txt','0002.txt','0003.txt','0004.txt','0005.txt','0006.txt'};


O4fields = {'0600000.txt', '0800000.txt', '1000000.txt', '1200000.txt','1400000.txt','1600000.txt','1800000.txt','2000000.txt','2200000.txt','2400000.txt'};


%m = matfile('/export/scratch2/diniz/HEAP2_data/HEAP_data_RREA.mat');
%loaded = m.HEAP_data_RREA;
%data = loaded.GEANT4O1;
%fields = fieldnames(data);




ftel = @(a,x)((x.^(a(1))).*exp(-x/a(2))); % non-linear fit model for photons
%opts = statset('TolFun',1e-30);
opts = statset('nlinfit');
opts.RobustWgtFun = 'bisquare';
%opts = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt');
j = [31 26 22 19 17 15 14 13 12 11 10 9 8]; % indices for times just before stop time
E_me = zeros(numel(fields),2); %array for fitted mean energy at all recorded distances and all electric fields
std_e = E_me; % array for the standard error on the fit
rel_err = E_me; % comparison for big errors : std_e/E_me
pw = E_me;
times = loaded.record_times(j+1); % the 12th element of stop_times there is no record_time in that time, so took other time instead
stop_distances = zeros(numel(loaded.stop_times),1);





%% parameters

c_light =  299792458      ;
m_elec  =  9.10938356e-31 ;
q_elec  =  1.60217662e-19 ;
bins=logspace(log10(10),log10(10^5),150);

%a1 = 1;
%a2 = 1;
%a3 = 10000;

%y = @(x) a1*((1./x).^a2).*exp(-x/a3); % non-linear fit model for photons



cp = 511;%cut-off for photons on  511 keV


for a = 1:numel(times)
    index = logical( loaded.record_times == times(a));
    temp = loaded.record_distances(index);
    stop_distances(a) = temp;
end




 
 %---------------------------GEANT4; FIRST O4 AND THEN O1 --------------------------------------
 
 
 for i = 1:numel(O4fields)
     file = strcat(filesO4_path,O4fields{i});
 
                   data_temp = importdata(file);
                     
                   type = data_temp(:,1);
                   zz = data_temp(:,4);
                   time = data_temp(:,8);
                   ener = data_temp(:,9);
                   
                   elecener = ener(type == 0);
                   elecdist = zz(type == 0);
                   
                   ide = logical((elecdist == stop_distances(i)).*(elecener >= cp));
 
 if sum(ide)>0
 
 
  sum(ide);
 % bins=logspace(log10(min(elecener(ide))),log10(max(elecener(ide))),120);
 
  [ne,edges]  = histcounts(elecener(ide),bins);
  centers = (edges(1:end-1) + edges(2:end))/2.0;
 difs = diff(edges);
 
 
 ne=ne./difs;
 ce = sum(ide);
 %ce = 1;
 ne = ne/ce;
 %centers = (edges(1:end-1) + edges(2:end))/2.0;
 fel = fitnlm(centers',ne',ftel,[-1 a3],'Options',opts);
 pw(i,1) = fel.Coefficients.Estimate(1);
 E_me(i,1) = fel.Coefficients.Estimate(2);
 std_e(i,1) = fel.Coefficients.SE(2);
 rel_err(i,1) = std_e(i,1)/E_me(i,1);
 y = @(x)(1./x).*exp(-x/E_me(i,1));
 fiel = num2str(loaded.field_list(i)*10^-6);
 
 spece= y(centers);
 
 dist = num2str(stop_distances(i));
 
 leg = strcat('E:', fiel,'[Mv/m]',',z:',dist,'[m]');
 
 
h(1) =   figure(1);
   loglog(centers,ne,'DisplayName',leg)
   legend('show','Location','SouthWest')
  
   hold on
    plot(centers,spece) 
  set(gca,'XScale','log')
   set(gca,'YScale','log')
  xlabel('Energy (keV)')
  ylabel('Counts per bin (keV-1)')
 
  title('GEANT4O4');
 grid on
 
 end
 end

for i = 1:numel(fields)
 elecener = data.(fields{i}).photon.energy;
 elecdist = data.(fields{i}).photon.z;


ide = logical((elecdist == stop_distances(i)).*(elecener >= cp));

if sum(ide)>0


 sum(ide);
% bins=logspace(log10(min(elecener(ide))),log10(max(elecener(ide))),120);

[ne,edges]  = histcounts(elecener(ide),bins);

centers = (edges(1:end-1) + edges(2:end))/2.0;
%E_me(i,2) = sum(ne.*centers)/sum(ide);
difs = diff(edges);

ne=ne./difs;
ce = sum(ide);
%ce = 1;
ne = ne/ce;
%centers = (edges(1:end-1) + edges(2:end))/2.0;

 fel = fitnlm(centers',ne',ftel,[-1 a3],'Options',opts);
 pw(i,2) = fel.Coefficients.Estimate(1);
 E_me(i,2) = fel.Coefficients.Estimate(2);
 std_e(i,2) = fel.Coefficients.SE(2);
 rel_err(i,2) = std_e(i,2)/E_me(i,2);
 y = @(x)(1./x).*exp(-x/E_me(i,2));

fiel = num2str(loaded.field_list(i)*10^-6);



dist = num2str(stop_distances(i));

leg = strcat('E:', fiel,'[Mv/m]',',z:',dist,'[m]');
spece = y(centers);

h(2) =   figure(2);
  loglog(centers,ne,'DisplayName',leg)
  legend('show','Location','SouthWest')
 
  hold on
 plot(centers,spece);
 
 set(gca,'XScale','log')
 set(gca,'YScale','log')
 xlabel('Energy (keV)')
 ylabel('Counts per bin (keV-1)')

 title('GEANT4O1');
grid on

end
end










%-----------------------------------E_vs_field---------------------------------------------------------------

h(3) = figure(3);     
     
       plot(loaded.field_list(1:numel(O4fields)),E_me(1:numel(O4fields),1),'DisplayName','GEANT4O4');
       hold on
       plot(loaded.field_list,E_me(:,2),'DisplayName','GEANT4O1');
      xlabel('Electric Field (V/M)');
     ylabel('Mean energy (keV)');
 
    legend('show','Location','south');

   
   
%------------------------------saving---------------------------------------------------------------------   
for f=1:3
figname = sprintf('phot_dist%02d', f);
%fig = sprintf('-f%d',f);
%print(fig,figname,'-dpdf')
savefig(h(f),figname);
end
