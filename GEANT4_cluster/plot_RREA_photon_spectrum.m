clear all
close all
clc


filename='/scratch/output/RREA_char/GEANT4O4_800000.txt';


data=importdata(filename);

times=data(:,8);
energies=data(:,9);

energies=energies(times==1.6000e-06);

bins=logspace(log10(min(energies)),log10(max(energies)),120);
[n,xout]=hist(energies,bins);

n=n./gradient(xout);


loglog(xout,n)

xlabel('Energy (keV)')
ylabel('Counts per bin (keV-1)')
grid on