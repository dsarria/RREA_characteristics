# -*- coding: utf-8 -*-

import subprocess
import numpy as np


############################################
def make_output_field_string(fieldvalue):

    field_str = str(int(fieldvalue))
    nb_digits = len(field_str)
    nb_digits_to_add = 8-nb_digits

    for _ in range(nb_digits_to_add):
        field_str = ''.join(('0',field_str))
    
    return field_str
############################################



commands = []

fields = np.array(range(4,32,2))*1.e5

physics_lists = ["O1"] 
#physics_lists = ["O1","O4"]

for physic_list in physics_lists :
    for field in fields:
        field_string = make_output_field_string(field)
        code_name = "GEANT4" + physic_list
        commands.append("cat ./output_RREA_100keV/output/O1/*_" + code_name + "_" + str(int(field)) + ".txt > " + code_name + "_" + field_string + ".txt")
 
print(commands)


for command in commands:
    try:
        subprocess.check_output(['bash','-c', command])
    except :
        print("Warning : could not run command : " + command)






        
