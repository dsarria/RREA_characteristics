clear all
close all
clc

fn='./data/fused.txt';

physics_list_names{1}='O1';
physics_list_names{2}='O4';

for jj=1:2

LIST_selected=physics_list_names{jj};
DIRECTION_selected='+1';

% IMPORTING
file = importdata(fn);
DIRECTION = file.textdata(:,2);
LIST = file.textdata(:,3); %('O1' or 'O4')


SEED = file.textdata(:,1);
E = file.data(:,2);
ENERGY = file.data(:,3)*1000; % to keV
N_AV = file.data(:,1);
PROB = file.data(:,4);



%% filtering physics list
SEED=SEED(strcmp(LIST,LIST_selected));
E=E(strcmp(LIST,LIST_selected));
ENERGY=ENERGY(strcmp(LIST,LIST_selected));
N_AV=N_AV(strcmp(LIST,LIST_selected));
PROB=PROB(strcmp(LIST,LIST_selected));
DIRECTION=DIRECTION(strcmp(LIST,LIST_selected));

%% filtering direction
SEED=SEED(strcmp(DIRECTION,DIRECTION_selected));
E=E(strcmp(DIRECTION,DIRECTION_selected));
ENERGY=ENERGY(strcmp(DIRECTION,DIRECTION_selected));
N_AV=N_AV(strcmp(DIRECTION,DIRECTION_selected));
PROB=PROB(strcmp(DIRECTION,DIRECTION_selected));



%% CALCULATING
Efield = unique(E);
Efield_grid=sort(Efield);

% adding 0 field (for cosmetics in the plot)
Efield_grid=[0;Efield_grid];

Ep = unique(ENERGY);
ENERGY_grid=sort(Ep);

n1 = length(Efield_grid);
n2 = length(ENERGY_grid);

prob_den{jj} = zeros(n1,n2);
[xx,yy] = meshgrid(Efield_grid,ENERGY_grid);



for i=1:n1
    for j=1:n2
        id = logical( (E==Efield_grid(i)).*(ENERGY==ENERGY_grid(j)) );
        
        if sum(id)>0
            
            stats_ini=N_AV(id);
            proba=sum(PROB(id).*stats_ini)/sum(stats_ini);
            
            similar_probas_lists{i,j}=PROB(id);
            
            prob_den{jj}(i,j)=proba;
            
        end
        
    end
end

% PLOTTING
figure(jj)
h{jj}=pcolor(yy,xx,prob_den{jj}');
set(h{jj}, 'EdgeColor', 'none');
colorbar
axis([0 1000 200000 3000000])
title(['Probability to get Relativistic Avalanche; ' LIST_selected])
xlabel('Primary energy (keV)')
ylabel('Electric field (V/m)')

end


%% relative difference O1 and O4 physics lists



rela_diff = abs(prob_den{2}-prob_den{1})./prob_den{2} .*100;
rela_diff(isnan(rela_diff))=0;
rela_diff(isinf(rela_diff))=0;

% PLOTTING
figure
h100=pcolor(yy,xx,rela_diff');
set(h100, 'EdgeColor', 'none');
colorbar

title(['Relative difference (%) between O1 and O4 physics lists'])
xlabel('Primary energy (keV)')
ylabel('Electric field (V/m)')


%% proba vs energy
figure

j=1
for i=[4 8 15 30 40 50]
    
    probas_plot = prob_den{1}(i,:);
    probas_plot(probas_plot<=1e-3)=1e-3;
    
    plot(ENERGY_grid,probas_plot)
    legendlabels{j}= [ 'E-Field :', num2str(round(Efield_grid(i)/1e5)), '*1e5 V/m'] ;
    xlabel('Energy (keV)')
    ylabel('Probability to get Relativistic Avalanche')
    hold on
    grid on
    j=j+1;
end
title('O1')
legend(legendlabels)

%% proba vs field
figure

j=1
for i=[10 15 18 25 40 50]
    
    probas_plot = prob_den{1}(:,i);
    probas_plot(probas_plot<=1e-3)=1e-3;
    
    plot(Efield_grid,probas_plot)
    legendlabels{j}= [ 'Energy :', num2str(round(ENERGY_grid(i))), ' keV'] ;
    xlabel('E-field (V/m)')
    ylabel('Probability to get Relativistic Avalanche')
    hold on
    grid on
    j=j+1;
end
title('O1')
legend(legendlabels)
