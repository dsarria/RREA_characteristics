clear all
close all
clc


%% if the maximum acceptable step is explictly given in the GEANT4 code (detector construction)
% and Energy theshold fixed to : gamma  10.058 keV,    e-  9.97664 keV,    e+  9.97664 keV
% O4 physics list

max_acc_step = [ 1      0.1     0.01   0.001    0.0001   0.00001] ; % meter
proba =        [ 65.2   88.2    92.7   91.1     78.44    54.43  ] ;


% if the maximum acceptable step is not explictly given in the GEANT4 code (detector construction)
% the probability of avalanche is 59.57 with energy thresholds
% (G4TransportationManager::GetTransportationManager()->GetPropagatorInField()->SetLargestAcceptableStep(XX*m);)

% with LBE physics list : 87.5



%% if energy theshold is changed, and  maximum acceptable step is not explictly given in the GEANT4 code (detector construction)

elec_ener_thes =  [9.976    3.14     0.99     0.99    0.99   0.99]; % keV
gamma_ener_thes = [10.05    7.53     5.68     4.28    1.51   0.99]; % keV
proba2 =          [59.7     74       78.3     83.3    80.3   81];
