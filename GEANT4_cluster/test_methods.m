clear all
close all
clc


nb_elec_ini =  [100 ];
E_field =      [8e5 ];
T_simu_max =   [200    400    600 ]; % ns

run_duration = [77     3429   ];
output_size =  [0.139  6.5    208 ] ;% MB