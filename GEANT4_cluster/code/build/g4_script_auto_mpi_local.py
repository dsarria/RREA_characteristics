#!/usr/bin/env python

import numpy as np
from subprocess import call
from functools import partial
from multiprocessing.dummy import Pool


def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)



# Initializations and preliminaries

#list_Efields= np.array(range(22,32,2))*1.e5 # V/m
#TIME_LIMITS = [1078.,  522., 351., 277.,  233.,  199.,  179.,   164.,  153.,  144.,  135., 130.,   124.] # nanoseconds

list_Efields= np.array(range(12,14,2))*1.e5 # V/m
TIME_LIMITS = [233.] # nanoseconds


nr_particles = 100 # per initial condition (except seed)
nb_particule_per_run =  100
nb_loops = nr_particles / nb_particule_per_run # number of run with same initial condition (except seed)


primary_energy=0.1  # MeV

list_ener_thres= [990.]               
list_physics= ['SS']
#list_physics= ['O1','LIV','PEN','O4']

step_max = 100 # mm
droverr = 0.2 # dimensionless

#list_Efields= np.array(range(2,22,2))*1.e5 # V/m

epstol=-5 # log10



################################################################################
# define the commands to be run in parallel
commands=[]

excecutable = './Base'
rand_seed = 1003

for physList in list_physics:
  for ener_t in list_ener_thres:
    for i_field in range(len(list_Efields)):
        for _ in range(int(nb_loops)):
            field = list_Efields[i_field]
            timeLim = TIME_LIMITS[i_field]
            commands.append(excecutable+' '+str(physList)+' '+str(nb_particule_per_run)+' '+str(field)+' '+str(epstol)+' '+str(primary_energy)+ ' ' + str(timeLim) + ' '+str(rand_seed)+' '+str(ener_t) +' '+str(step_max) + ' ' + str(droverr) ) 
            rand_seed += 1

################################################################################




nb_thread = 1 # number of threads (cpu) to run

# Making an array where each element is the list of command for a given thread

command_number = len(commands)

print('Number of commands required '+ str(command_number))

pool = Pool(nb_thread) # to be always set to 1 for this MPI case
for i, returncode in enumerate(pool.imap(partial(call, shell=True), commands)):
    if returncode != 0:
        print("%d command failed: %d" % (i, returncode))


