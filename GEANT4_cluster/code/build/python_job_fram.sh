#!/bin/bash
## Project:
#SBATCH --account=NN9526K
## Job name:
#SBATCH --job-name=RREA_char
#SBATCH -p normal
## Wall time limit:
#SBATCH --time=7-0:0:0
## Number of nodes and task er node
#SBATCH --nodes=20 --ntasks-per-node=32
#SBATCH --mail-user=david.sarria@uib.no


## Recommended safety settings:
set -o errexit # Make bash exit on any error
set -o nounset # Treat unset variables as errors

## Software modules
module restore system   # Restore loaded modules to the default

module load foss/2017a
module load CMake/3.9.1
module load GCCcore/6.4.0
module load Python/2.7.13-intel-2017a
module load Boost/1.63.0-intel-2017a-Python-2.7.13

module list             # List loaded modules, for easier debugging


## Run the application

cd /cluster/work/users/dsarria/G4_projects/RREA_char/build/

srun python g4_script_auto_mpi.py

