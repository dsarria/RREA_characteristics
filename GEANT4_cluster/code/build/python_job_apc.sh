#!/bin/bash
#PBS -N python_geant4_fluc
#PBS -o /home/dsarria/geant4_projects/HEAPS/RREA_other_run/RREA_characteristics/GEANT4_cluster/code/build/out_file
#PBS -e /home/dsarria/geant4_projects/HEAPS/RREA_other_run/RREA_characteristics/GEANT4_cluster/code/build/err_file
#PBS -q furious
#PBS -M dsarria@apc.in2p3.fr
#PBS -l nodes=1:ppn=32,mem=64gb,walltime=480:00:00

export SCRATCH="/scratch/$USER.$PBS_JOBID" 
export PATH=/usr/local/openmpi/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/openmpi/lib/:/usr/local/openmpi/lib/openmpi/:$LD_LIBRARY_PATH


cd /home/dsarria/geant4_projects/HEAPS/RREA_other_run/RREA_characteristics/GEANT4_cluster/code/build
mpirun -np 32 python g4_script_auto_mpi.py
