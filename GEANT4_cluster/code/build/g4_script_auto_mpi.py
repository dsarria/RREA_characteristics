#!/usr/bin/env python

import numpy as np
from subprocess import call
from mpi4py import MPI
from functools import partial
from multiprocessing.dummy import Pool


def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)



# Initializations and preliminaries
comm = MPI.COMM_WORLD   # get MPI communicator object
size = comm.Get_size()       # total number of processes
rank = comm.Get_rank()       # rank of this process
status = MPI.Status()   # get MPI status object

#list_Efields= np.array(range(6,32,2))*1.e5 # V/m
#TIME_LIMITS = [1078.,  522., 351., 277.,  233.,  199.,  179.,   164.,  153.,  144.,  135., 130.,   124.] # nanoseconds
list_Efields= np.array(range(6,30,2))*1.e5 # V/m
TIME_LIMITS = [1078.,  522., 351., 277.,  233.,  199.,  179.,   164.,  153.,  144.,  135., 130.] # nanoseconds

nb_threads = size-1

nr_particles = 200 # per initial condition (except seed)
nb_particule_per_run =  1
nb_loops = nr_particles / nb_particule_per_run # number of run with same initial condition (except seed)


primary_energy=0.1  # MeV

list_ener_thres= [990.]               
list_physics= ['O1','O4']
#list_physics= ['O1','LIV','PEN','O4']


#list_Efields= np.array(range(2,22,2))*1.e5 # V/m

epstol=-5 # log10



################################################################################
# define the commands to be run in parallel
commands=[]

excecutable = './Base'
rand_seed = 1



for physList in list_physics:
  for ener_t in list_ener_thres:
    for i_field in range(len(list_Efields)):
        for _ in range(int(nb_loops)):
            field = list_Efields[i_field]
            timeLim = TIME_LIMITS[i_field]
            commands.append(excecutable+' '+str(physList)+' '+str(nb_particule_per_run)+' '+str(field)+' '+str(epstol)+' '+str(primary_energy)+ ' ' + str(timeLim) + ' '+str(rand_seed)+' '+str(ener_t)) 
            rand_seed += 1

################################################################################




# Define MPI message tags
tags = enum('READY', 'DONE', 'EXIT', 'START')



if rank == 0:
    # Master process executes code below
    tasks = commands
    task_index = 0
    num_workers = size - 1
    closed_workers = 0
    print("Master starting with %d workers" % num_workers)
    while closed_workers < num_workers:
        data = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
        source = status.Get_source()
        tag = status.Get_tag()
        if tag == tags.READY:
            # Worker is ready, so send it a task
            if task_index < len(tasks):
                comm.send(tasks[task_index], dest=source, tag=tags.START)
                print("Sending task %d to worker %d" % (task_index, source))
                task_index += 1
            else:
                comm.send(None, dest=source, tag=tags.EXIT)
        elif tag == tags.DONE:
            results = data
            print("Got data from worker %d" % source)
        elif tag == tags.EXIT:
            print("Worker %d exited." % source)
            closed_workers += 1

    print("Master finishing")
else:
    # Worker processes execute code below
    name = MPI.Get_processor_name()
    print("I am a worker with rank %d on %s." % (rank, name))
    while True:
        comm.send(None, dest=0, tag=tags.READY)
        task = comm.recv(source=0, tag=MPI.ANY_TAG, status=status)
        tag = status.Get_tag()
        
        if tag == tags.START:
            # Do the work here
            task2=[task]
            pool = Pool(1) # to be always set to 1 for this MPI case
            for i, returncode in enumerate(pool.imap(partial(call, shell=True), task2)):
                if returncode != 0:
                    print("%d command failed: %d" % (i, returncode))
            comm.send(returncode, dest=0, tag=tags.DONE)
        elif tag == tags.EXIT:
            break

comm.send(None, dest=0, tag=tags.EXIT)


