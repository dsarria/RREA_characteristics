#!/bin/bash
#PBS -N python_mpi_geant4_ILDAS
#PBS -A fysisk
#PBS -o /work/dsarria/G4_projects/RREA_char/GEANT4_cluster/code/build/out_file
#PBS -e /work/dsarria/G4_projects/RREA_char/GEANT4_cluster/code/build/err_file
#PBS -q normal
#PBS -M david.sarria@uib.no
#PBS -l walltime=200:00:00,mppwidth=300

module list
module load gcc/6.1.0
module load python/2.7.9-dso
module load cmake/3.5.2
module load boost/1.60.0


cd /work/dsarria/G4_projects/RREA_char/GEANT4_cluster/code/build/
aprun -B g4_script_auto_mpi.py
