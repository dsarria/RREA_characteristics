import paramiko as para
import numpy as np
import zipfile
import os
import time

## Main parameters

project_name = "RREA_char"

source_folder = "./src/"
build_folder = "./build/" # local and remote
path_to_project_root = "./"
remote_projects_path = "/cluster/work/users/dsarria/G4_projects/"

slurm_batch_file = "python_job_fram.sh"
python_geant4_file = "g4_script_auto_mpi.py"

username = "dsarria"
host = "fram.sigma2.no"

nb_nodes = 20
nb_task_per_node = 32

### batch job sample

batch_script = """#!/bin/bash
## Project:
#SBATCH --account=NN9526K
## Job name:
#SBATCH --job-name={}
#SBATCH -p normal
## Wall time limit:
#SBATCH --time=7-0:0:0
## Number of nodes and task er node
#SBATCH --nodes={} --ntasks-per-node={}
#SBATCH --mail-user=david.sarria@uib.no


## Recommended safety settings:
set -o errexit # Make bash exit on any error
set -o nounset # Treat unset variables as errors

## Software modules
module restore system   # Restore loaded modules to the default

module load foss/2017a
module load CMake/3.9.1
module load GCCcore/6.4.0
module load Python/2.7.13-intel-2017a
module load Boost/1.63.0-intel-2017a-Python-2.7.13

module list             # List loaded modules, for easier debugging


## Run the application

cd /cluster/work/users/{}/G4_projects/{}/build/

srun python {}

""".format(project_name, nb_nodes, nb_task_per_node, username, project_name, python_geant4_file)


### functions

def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            if ("output" not in root) and ("gamma" not in os.path.join(root, file)) and (file != "mos_test") and (
                    not (file.endswith("cbp"))):
                ziph.write(os.path.join(root, file))


##
def execute_command_on_remote(sshclient, command1):
    (stdin, stdout, stderr) = sshclient.exec_command(command1)

    for line in stdout.readlines():
        if "warning" not in line:
            print(line)


#######################################################################################################################

if __name__ == '__main__':
    #print(batch_script)

    # rewritting batch script
    wr = open(build_folder + slurm_batch_file, 'w')  # opening and deleting
    wr.write(batch_script)
    wr.close()

    # ziping
    print("Compressing source and build forders...")

    source_zipfile = "G4project_src.zip"
    build_zipfile = "G4project_build.zip"

    zipf = zipfile.ZipFile(path_to_project_root + source_zipfile, 'w', zipfile.ZIP_DEFLATED)
    zipdir(source_folder, zipf)
    zipf.close()

    zipf = zipfile.ZipFile(path_to_project_root + build_zipfile, 'w', zipfile.ZIP_DEFLATED)
    zipdir(build_folder, zipf)
    zipf.close()

    print("Done!")

    ##

    remote_project_folder = remote_projects_path + project_name + "/"

    port = 22

    # connecting to ssh client
    sshclient = para.SSHClient()
    sshclient.load_system_host_keys()
    sshclient.connect(host, port, username)

    # removing/cleaning project directory
    command0 = "rm -rf " + remote_project_folder
    execute_command_on_remote(sshclient, command0)
    print("project directory cleaned.")

    # connecting to sftp client
    sftpclient = sshclient.open_sftp()

    # creating project directory
    cd_folder = "cd " + remote_projects_path + ";"

    command = cd_folder + "mkdir " + project_name
    execute_command_on_remote(sshclient, command)

    # copying zip files
    print("copying zip files into cluster...")
    sftpclient.put(path_to_project_root + source_zipfile, remote_project_folder + source_zipfile)

    sftpclient.put(path_to_project_root + build_zipfile, remote_project_folder + build_zipfile)

    print("Copying done.")

    sftpclient.close()

    # unziping folders
    print("Unziping into cluster...")
    command1 = "unzip " + remote_project_folder + source_zipfile + " -d " + remote_project_folder
    execute_command_on_remote(sshclient, command1)

    command2 = "unzip " + remote_project_folder + build_zipfile + " -d " + remote_project_folder
    execute_command_on_remote(sshclient, command2)
    print("Unziping done.")

    #
    remote_build_folder = remote_project_folder + build_folder

    # running cmake and compiling
    cd_build_folder = "cd " + remote_build_folder + ";"

    command = cd_build_folder + "rm -rf CMakeCache.txt"
    execute_command_on_remote(sshclient, command)
    print("removed CMakeCache.txt")

    command = cd_build_folder + "mkdir output/"
    execute_command_on_remote(sshclient, command)
    print("Created output directory.")

    print("Executing CMake...")
    command = cd_build_folder + "cmake ../src/"
    execute_command_on_remote(sshclient, command)
    print("CMake executed.")

    print("Compiling code...")
    command = cd_build_folder + "make -j8"
    execute_command_on_remote(sshclient, command)
    print("Code compiled.")

    ##

    print("Submitting batch job...")
    command = cd_build_folder + " sbatch " + slurm_batch_file
    execute_command_on_remote(sshclient, command)
    print("Batch job submitted")

    time.sleep(0.1)

    sshclient.close()

    print("Connexion to Fram closed.")
