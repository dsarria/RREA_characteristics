//

// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************


// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#include "BaseSteppingAction.hh"
#include "BaseRunAction.hh"
#include "BaseDetectorConstruction.hh"

#include "G4SteppingManager.hh"
#include "G4VTouchable.hh"
#include "G4VPhysicalVolume.hh"


#include "G4SystemOfUnits.hh"
#include <iomanip>
#include <iostream>
#include <fstream>


extern std::vector<G4double> RECORD_TIMES;
extern std::vector<G4double> RECORD_DISTANCES;


// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BaseSteppingAction::BaseSteppingAction(BaseDetectorConstruction *det)
    : Detector(det)
{}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BaseSteppingAction::~BaseSteppingAction()
{}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....


void BaseSteppingAction::UserSteppingAction(const G4Step *aStep)

{
    G4Track *aTrack = aStep->GetTrack(); // the track is the full trajectory of the particle.

    //The step is just a part of a track, from a point A to a point B

    const G4double time_pre  = (aStep->GetPreStepPoint()->GetGlobalTime()); // time at point A
    const G4double time_post = (aStep->GetPostStepPoint()->GetGlobalTime()); // time at point B
    
    if (Settings::USE_STACKING_ACTION) {
      if (time_pre > Settings::VARIABLE_TIME_LIM)
      {
          aTrack->SetTrackStatus(fSuspend);
      }
    }

    const G4double ener_pre  = (aStep->GetPreStepPoint()->GetKineticEnergy()); // energy at point A
//     const G4double ener_post = (aStep->GetPostStepPoint()->GetKineticEnergy()); // energy at point B
    
    if (Settings::REMOVE_BELOW_10KEV) {
    // removing particles below 10 keV for faster code
      if (ener_pre < 10.0 * keV)
          {
              aTrack->SetTrackStatus(fStopAndKill);
              return;
          }
    }

    const G4double z_pre  = (aStep->GetPreStepPoint()->GetPosition().z()); // Z position at point A
    const G4double z_post = (aStep->GetPostStepPoint()->GetPosition().z()); // Z position at point B

    // Particle ID
    const G4int PDG_nb = aTrack->GetDefinition()->GetPDGEncoding();
    // using PDG number is prefered instead of name for performance

    //  22 -> gamma
    //  11 -> e-
    // -11 -> e+
    //    if (PDG_nb == 22) // for photons
    //        {
    //            G4cout << "lala" << G4endl;
    //            G4cout << PDG_nb << G4endl;
    //            G4cout << ener_pre << G4endl;
    //            G4cout << aTrack->GetKineticEnergy() << G4endl;
    //            G4cout << ener_post << G4endl;
    //            G4cout << "lala" << G4endl;
    //        }

    // particle type check
    if (!((PDG_nb == 22) || (PDG_nb == 11) || (PDG_nb == -11))) // get out of this function if not photon, electron or positron
        {
            return;
        }

    // Time limit check
    if (time_pre > Settings::TIME_LIMIT) // kill the track and get out of function if time is over the limit
        {
            aTrack->SetTrackStatus(fStopAndKill);
            return;
        }

    // Time record check
    for (const double & record_time : RECORD_TIMES_copy) // check all times
        {
            if ((time_pre <= record_time) && (time_post >= record_time)) // if current times at point A and B surround a record time
                {
                    //
                    const G4int id_output = get_id_output(PDG_nb);

                    // interp energy

                    G4double interp_energy = 0.;

                    const G4ThreeVector pos = aTrack->GetPosition() / meter;
                    const G4double interp_Z_pos =  interp_property(z_pre, z_post, time_pre, time_post, record_time);
                    const G4double x        = pos.x();
                    const G4double y        = pos.y();
                    const G4double z        = interp_Z_pos / meter;

                    if (PDG_nb == 22) // for photons
                        {
                            interp_energy = ener_pre;
                        }
                    else
                        {
                            interp_energy =  ener_pre + double(id_output) * Settings::EF_SIGN * (interp_Z_pos - z_pre) * E_field_MeV_mm;
                            //                            interp_energy =  interp_property(ener_pre, ener_post, time_pre, time_post, record_time);
                        }

                    // getting mometum and in the good unit
                    const G4ThreeVector mom_dir = aTrack->GetMomentumDirection();
                    const G4ThreeVector mom_si = calc_momentum_SI(PDG_nb, mom_dir, interp_energy);

                    const G4double time_output = record_time / second;

                    const G4double energy_output = interp_energy / keV;

                    if (energy_output >= MinEner_copy)
                        {
                            analysis->output_particle(id_output, x, y, z, mom_si.x(), mom_si.y(), mom_si.z(), time_output, energy_output);
                        }
                }
        }

    // distance record check (same technique as for time records)
    for (const double & record_dist : RECORD_DISTANCES_copy)
        {
            if (((z_pre <= record_dist) && (z_post >= record_dist)) || ((z_pre >= record_dist) && (z_post <= record_dist)))
                {
                    //
                    const G4int id_output = get_id_output(PDG_nb);

                    // interp energy
                    G4double interp_energy = 0.;

                    if (PDG_nb == 22) // for photons
                        {
                            interp_energy = ener_pre;
                        }
                    else
                        {
                            interp_energy = ener_pre + double(id_output) * Settings::EF_SIGN * (record_dist - z_pre) * E_field_MeV_mm;
                            //                            interp_energy =  interp_property(ener_pre, ener_post, z_pre, z_post, record_dist);
                        }

                    // getting mometum and in the good unit
                    const G4ThreeVector mom_dir = aTrack->GetMomentumDirection();

                    const G4ThreeVector mom_si =  calc_momentum_SI(PDG_nb, mom_dir, interp_energy);

                    const G4double time_output = aTrack->GetGlobalTime() / second;

                    const G4double energy_output = interp_energy / keV;

                    const G4ThreeVector pos = aTrack->GetPosition() / meter;
                    const G4double x        = pos.x();
                    const G4double y        = pos.y();

                    const G4double z2 = record_dist / meter;

                    if (energy_output >= MinEner_copy)
                        {
                            analysis->output_particle(id_output, x, y, z2, mom_si.x(), mom_si.y(), mom_si.z(), time_output, energy_output);
                        }
                }
        }
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

G4ThreeVector BaseSteppingAction::calc_momentum_SI(const G4int PDG_nb, const G4ThreeVector &mom_dir, const G4double &average_energy)
{
    G4ThreeVector ppp(0., 0., 0.);

    // getting mometum and in the good unit
    if (PDG_nb == 22)
        {
            const G4double ener_joules = average_energy / joule;
            const G4double c_light_SI  = CLHEP::c_light / (m / s);
            ppp = mom_dir * ener_joules / c_light_SI;
        }
    else
        {
            const G4double ener_joules     = average_energy / joule;
            const G4double c_light_SI      = CLHEP::c_light / (m / s);
            const G4double elec_mass_c2_SI = CLHEP::electron_mass_c2 / joule;
            const G4double factor          = std::sqrt(ener_joules * (ener_joules + 2. * elec_mass_c2_SI)) / c_light_SI;
            ppp = mom_dir * factor;
        }

    return ppp; // in SI units
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

G4int BaseSteppingAction::get_id_output(const G4int PDG_nb)
{
    G4int id_output = 0;

    if (PDG_nb == 11)
        {
            id_output = -1;
        }
    else if (PDG_nb == -11)
        {
            id_output = 1;
        }
    else if (PDG_nb == 22)
        {
            id_output = 0;
        }

    return id_output;
}

// linear interpolation
G4double BaseSteppingAction::interp_property(const G4double ener_pre, const G4double ener_post, const G4double time_pre, const G4double time_post, const G4double record_time)
{
    return (record_time - time_pre) * (ener_post - ener_pre) / (time_post - time_pre) + ener_pre;
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
