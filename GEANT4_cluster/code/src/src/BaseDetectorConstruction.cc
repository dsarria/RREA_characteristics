//

// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

//
#include "BaseDetectorConstruction.hh"
#include "globals.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4PVParameterised.hh"
#include "G4Mag_UsualEqRhs.hh"
#include "G4FieldManager.hh"
#include "G4UniformElectricField.hh"
#include "G4TransportationManager.hh"
#include "G4EqMagElectricField.hh"
#include "G4ElectroMagneticField.hh"

#include "G4TransportationManager.hh"
#include "G4PropagatorInField.hh"

#include "G4ChordFinder.hh"
#include "G4UniformMagField.hh"
#include "G4ExplicitEuler.hh"
#include "G4ImplicitEuler.hh"
#include "G4SimpleRunge.hh"
#include "G4SimpleHeum.hh"
#include "G4ClassicalRK4.hh"
#include "G4HelixExplicitEuler.hh"
#include "G4HelixImplicitEuler.hh"
#include "G4HelixSimpleRunge.hh"
#include "G4CashKarpRKF45.hh"
#include "G4RKG3_Stepper.hh"

#include "G4UserLimits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4UnitsTable.hh"
#include "G4ios.hh"

#include "G4ExactUniEFStepper.hh"


#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/irange.hpp>


// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
// Possibility to turn off (0) magnetic field and measurement volume.
// #define Layer 1          // Magnet geometric volume
#define MAG 1        // Magnetic field grid
#define MEASUREVOL 1 // Volume for measurement
// #define DETECTORVOL 1  // Volume for detectors

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

extern double MIN_STEP;
extern double EFIELD;
extern double LOG10_EPSTOL;


BaseDetectorConstruction *BaseDetectorConstruction::instance = 0;

BaseDetectorConstruction::BaseDetectorConstruction()
    : physiWorld(NULL), logicWorld(NULL), solidWorld(NULL)

{

}

BaseDetectorConstruction *BaseDetectorConstruction::getInstance()
{
    if (instance == 0) instance = new BaseDetectorConstruction;

    return instance;
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BaseDetectorConstruction::~BaseDetectorConstruction()
{}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

G4VPhysicalVolume *BaseDetectorConstruction::Construct()

{
    DefineMaterials();

    return ConstructWorld();
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BaseDetectorConstruction::DefineMaterials()
{
    G4String name;
    G4double density;

    G4int ncomponents;
    G4double fractionmass;

    // Define Elements
    G4Element *elN  = new G4Element("Nitrogen", "N",  7., 14.01 * g / mole);
    G4Element *elO  = new G4Element("Oxygen", "O",  8., 16.00 * g / mole);
    G4Element *elAr = new G4Element("Argon", "Ar",  18., 39.948 * g / mole);

    // Define Material from elements.
    // Example: G4Material* Notation = new G4Material("Material", atomic number z,
    // mass of mole a, density);


    density = 1.293   * kg / m3;
    Air = new G4Material(name = "air_" + std::to_string(0), density, ncomponents = 3);
    Air->AddElement(elN, fractionmass = 0.755);
    Air->AddElement(elO, fractionmass = 0.2322);
    Air->AddElement(elAr, fractionmass = 0.0128);

    // Default materials in setup.

    MeasureMaterial = Air;
}



G4VPhysicalVolume *BaseDetectorConstruction::ConstructWorld()
{

    // E-field
    create_Efield();

    // ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
    //
    // World
    //


    G4double cylinderHeight = 10. * km;
    G4double cylinderRadius = 5. * km;

    solidWorld = new G4Tubs("WorldS",
                            0., cylinderRadius, cylinderHeight / 2., 0. * degree, 360. * degree);


    logicWorld = new G4LogicalVolume(solidWorld, MeasureMaterial, "WorldL");
    logicWorld->SetFieldManager(globalfieldMgr, true);


    physiWorld = new G4PVPlacement(0,
                                   G4ThreeVector(0., 0., 0.),
                                   "WorldP",
                                   logicWorld,
                                   NULL,
                                   false,
                                   0);

    return physiWorld;
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BaseDetectorConstruction::create_Efield()
{
    G4double MinStep = Settings::MIN_STEP * micrometer; // minimal step, micrometer

    G4double minEps = pow(10., Settings::LOG10_EPSTOL); //   Minimum & value for smallest steps
    G4double maxEps = pow(10., Settings::LOG10_EPSTOL);

    // Create an equation of motion for glubal field (zero)
    G4ElectricField *myEfield = new G4UniformElectricField(G4ThreeVector(0.0, 0.0, Settings::EF_SIGN * Settings::EFIELD));

    G4EqMagElectricField *Equation = new G4EqMagElectricField(myEfield);

    G4int nvar = 8; // to integrate time and energy

    G4MagIntegratorStepper *EStepper = new G4ClassicalRK4(Equation, nvar);
    //     G4MagIntegratorStepper* EStepper = new G4ExactUniEFStepper(Equation);

    // Get the global field manager
    globalfieldMgr = G4TransportationManager::GetTransportationManager()->GetFieldManager();

    // Set this field to the global field manager
    globalfieldMgr->SetDetectorField(myEfield);


    G4MagInt_Driver *EIntgrDriver = new G4MagInt_Driver(MinStep,
            EStepper,
            EStepper->GetNumberOfVariables());

    G4ChordFinder *EChordFinder = new G4ChordFinder(EIntgrDriver);
    globalfieldMgr->SetChordFinder(EChordFinder);


    globalfieldMgr->SetMinimumEpsilonStep(minEps);
    globalfieldMgr->SetMaximumEpsilonStep(maxEps);


    globalfieldMgr->SetDeltaOneStep(1.e-3 * mm);


    // NO MORE USED, SETTING UP A MAX STEP IN THE PHYSICS LIST IS PREFERED
    //  G4TransportationManager::GetTransportationManager()->GetPropagatorInField()->SetLargestAcceptableStep(1. * cm);


}



//// not used anymore

// Putting air cylinders layers inside world (air) cylinder
// (usefull to record particles when they cross given distances)


//    std::vector<G4double> cylinder_layers_dists;

// range of distances, meter by meter
//    boost::push_back(cylinder_layers_dists, boost::irange(int(-100), int(3840 * meter), int(1 * meter)));


//    std::vector < G4Tubs * >  airCylS;
//    std::vector < G4LogicalVolume * >  airCylL;
//    std::vector < G4VPhysicalVolume * >  airCylP;

//    G4double cylinderCenterZ;

//    for (unsigned int ii = 0; ii < cylinder_layers_dists.size() - 1; ii++)
//        {
//            //       G4cout << cylinder_layers_dists[ii] << G4endl;

//            cylinderHeight = (cylinder_layers_dists[ii + 1] - cylinder_layers_dists[ii]);

//            airCylS.push_back(new G4Tubs("airCylS" + std::to_string(ii),
//                                         0., cylinderRadius, cylinderHeight / 2., 0. * degree, 360. * degree));


//            airCylL.push_back(new G4LogicalVolume(airCylS[ii],
//                                                  MeasureMaterial,
//                                                  "airCylL" + std::to_string(ii)));

////            airCylL.back()->SetFieldManager(localfieldMgr, true);


//            cylinderCenterZ = cylinder_layers_dists[ii];

//            airCylP.push_back(new G4PVPlacement(0,
//                                                G4ThreeVector(0., 0., cylinderCenterZ),
//                                                "airCylP" + std::to_string(ii),
//                                                airCylL[ii],
//                                                physiWorld,
//                                                false,
//                                                0, 0));
//        }



// Create an equation of motion for local field
//    G4ElectricField *myLocalEfield      = new G4UniformElectricField(G4ThreeVector(0.0, 0.0, -1.*Settings::EFIELD));
//    G4EqMagElectricField *EquationLocal = new G4EqMagElectricField(myLocalEfield);

//    G4MagIntegratorStepper *EStepper2 = new G4ClassicalRK4(EquationLocal, nvar);

//     G4MagIntegratorStepper* EStepper2 = new G4ExactUniEFStepper(
// EquationLocal );


//    localfieldMgr->SetDetectorField(myLocalEfield);
//    G4MagInt_Driver *EIntgrDriver2 = new G4MagInt_Driver(MinStep,
//            EStepper2,
//            EStepper2->GetNumberOfVariables());
//    G4ChordFinder *EChordFinder2 = new G4ChordFinder(EIntgrDriver2);
//    localfieldMgr->SetChordFinder(EChordFinder2);


//    localfieldMgr->SetMinimumEpsilonStep(minEps); //
//    localfieldMgr->SetMaximumEpsilonStep(maxEps);
//    localfieldMgr->SetDeltaOneStep(1.e-3 * mm);

