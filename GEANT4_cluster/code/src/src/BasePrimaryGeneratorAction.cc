//

// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************


// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#include "BasePrimaryGeneratorAction.hh"

#include "BaseDetectorConstruction.hh"

#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"

#include "Randomize.hh"

// Print position of primaries.
#define POSITION 0

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....



BasePrimaryGeneratorAction::BasePrimaryGeneratorAction()
{
    // default kinematic
    G4int n_particle = 1;

    particleGun = new G4ParticleGun(n_particle);


    if (Settings::TYPE == 0)
        {
            particle = G4ParticleTable::GetParticleTable()->FindParticle("gamma");
        }
    else if (Settings::TYPE == -1)
        {
            particle = G4ParticleTable::GetParticleTable()->FindParticle("e-");
        }
    else if (Settings::TYPE == 1)
        {
            particle = G4ParticleTable::GetParticleTable()->FindParticle("e+");
        }
    else
        {
            G4cout << "ERROR : NOT A VALID PARTICLE TYPE (not O, -1 or 1)" << G4endl;
        }

}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BasePrimaryGeneratorAction::~BasePrimaryGeneratorAction()
{
    delete particleGun;
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BasePrimaryGeneratorAction::GeneratePrimaries(G4Event *anEvent)
{

    particleGun->SetParticleDefinition(particle);
    particleGun->SetParticleEnergy(Settings::PRIMARY_ENERGY * MeV);

    // Momentum Direction
    particleGun->SetParticleMomentumDirection(G4ThreeVector(0.0, 0.0, 1.0));


    particleGun->SetParticlePosition(G4ThreeVector(0., 0., 0.));
    particleGun->SetParticleTime(0.);


    particleGun->GeneratePrimaryVertex(anEvent);
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
