#include "BaseRunAction.hh"

#include "G4Run.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

#include "G4UnitsTable.hh"
#include "G4Element.hh"
#include "G4Material.hh"
#include "G4ParticleTable.hh"
#include "G4EmCalculator.hh"

#include <time.h>
#include <sys/time.h>


BaseRunAction::BaseRunAction()
{
    saveRndm = 1;

}

BaseRunAction::~BaseRunAction()
{}


void   BaseRunAction::BeginOfRunAction(const G4Run *)
{}

void   BaseRunAction::EndOfRunAction(const G4Run *)
{

    analysis->write_output_endOfRun();

}

