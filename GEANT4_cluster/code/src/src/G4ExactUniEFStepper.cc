//

// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: G4ExactUniEFStepper.cc 66356 2012-12-18 09:02:32Z gcosmo $
//
//  Helix a-la-Explicity Euler: x_1 = x_0 + helix(h)
//   with helix(h) being a helix piece of length h
//   simplest approach for solving linear differential equations.
//  Take the current derivative and add it to the current position.
//
//  As the field is assumed constant, an error is not calculated.
//
//  Author: D. Sarria, 20 Dec 2016
//     Implementation adapted from G4ExactHelixStepper of J. Apostolakis
// -------------------------------------------------------------------


// NOT WORKING YET !!!!!


#include "G4ExactUniEFStepper.hh"
#include "G4PhysicalConstants.hh"
#include "G4ThreeVector.hh"
#include "G4LineSection.hh"
#include "G4Mag_EqRhs.hh"

G4ExactUniEFStepper::G4ExactUniEFStepper(G4EqMagElectricField *EqMagElectricField)
    :  G4MagIntegratorStepper(EqMagElectricField, 8)
{}

G4ExactUniEFStepper::~G4ExactUniEFStepper() {}

void
G4ExactUniEFStepper::Stepper(const G4double yInput[],
                             const G4double *,
                             G4double       hstep,
                             G4double       yOut[],
                             G4double       yErr[])
{
    const G4int nvar = 8; // position, momentum, energy and time

    G4int i;
    G4ThreeVector Efld_value;

    G4double yOut2[nvar];

    ElecFieldEvaluate(yInput, Efld_value);


    AdvanceParabola(yInput, Efld_value, hstep, yOut);

    fInitialPoint = G4ThreeVector(yInput[0],   yInput[1],   yInput[2]);
    fFinalPoint   = G4ThreeVector(yOut[0],   yOut[1],   yOut[2]);


    AdvanceParabola(yInput, Efld_value, hstep / 2., yOut2);

    fMidPoint = G4ThreeVector(yOut2[0],   yOut2[1],   yOut2[2]);

    // We are assuming a constant E field: parabola is exact
    //
    for (i = 0; i < nvar; i++)
        {
            yErr[i] = 0.0;
        }
}

void G4ExactUniEFStepper::DumbStepper(const G4double yIn[],
                                      G4ThreeVector  Efld,
                                      G4double       h,
                                      G4double       yOut[])
{
    // Assuming a constant E-field: solution is a parabola

    AdvanceParabola(yIn, Efld, h, yOut);

    G4Exception("G4ExactUniEFStepper::DumbStepper",
                "GeomField0002", FatalException,
                "Should not be called. Stepper must do all the work.");
}

// ---------------------------------------------------------------------------

G4double G4ExactUniEFStepper::DistChord() const
{
    ////////////////////////////////////////////////////////////////
    //
    // Estimate the maximum distance from the curve to the chord
    //
    // We estimate this using the distance of the midpoint to chord.
    // The method below is good only for angle deviations < 2 pi;
    // this restriction should not be a problem for the Runge Kutta methods,
    // which generally cannot integrate accurately for large angle deviations

    G4double distLine, distChord;

    if (fInitialPoint != fFinalPoint)
        {
            distLine = G4LineSection::Distline(fMidPoint, fInitialPoint, fFinalPoint);

            // This is a class method that gives distance of Mid
            //  from the Chord between the Initial and Final points.

            distChord = distLine;
        }
    else
        {
            distChord = (fMidPoint - fInitialPoint).mag();
        }

    //   G4cout << distChord << G4endl;

    return distChord;
}

void G4ExactUniEFStepper::AdvanceParabola(const G4double      yIn[],
        const G4ThreeVector Efld,
        const G4double      h,
        G4double            yParabola[])
{
    G4double Ini_Energy = yIn[6];


    G4ThreeVector initMomentum = G4ThreeVector(yIn[3], yIn[4], yIn[5]); // maybe
    // not a
    // momentum

    const G4double initMomentum_mag = initMomentum.mag();

    G4ThreeVector direction = initMomentum / initMomentum_mag;

    G4double gamma = 1. + Ini_Energy / electron_mass_c2;
    G4double beta  = sqrt(1.0 - std::pow(gamma, -2));


    G4ThreeVector initVelocity = beta * c_light * direction; // velocity in GEANT4
    // units

    // initVelocity.mag() = v
    // initMomentum_mag = gamma * m * v

    const G4double particle_mass = initMomentum_mag / initVelocity.mag() / gamma; //
    //
    // mass
    //
    // of
    //
    // the
    //
    // particle
    //
    // in
    //
    // unkown
    //
    // units


    G4double velocityVal = initVelocity.mag();


    G4double Emag = Efld.mag();

    G4ThreeVector E_hat = (1.0 / Emag) * Efld;


    // parallel and perp vectors

    G4ThreeVector vpar = E_hat.dot(initVelocity) * E_hat; // the component
    // parallel to E
    G4ThreeVector vperp = initVelocity - vpar;            // the component
    // perpendicular to E


    G4double mag_vpar = vpar.mag();

    // to get charge (-1 ou 1 due to the GEANT4 system of unit)
    G4double dydx[8];

    RightHandSide(yIn, dydx);
    G4double particleChargeSign = (dydx[5] * velocityVal / Efld[2]) / (std::abs(dydx[5] * velocityVal / Efld[2])); //
    //
    // +1
    //
    // or
    //
    // -1
    //

    G4double aa = particleChargeSign * eplus * Emag / Ini_Energy;


    // time step corresponding to spatial step h
    G4double delta_time = log(aa * h + 1.0) / (aa * mag_vpar);

    G4double act = aa * c_light * delta_time;

    G4double cosh_act = std::cosh(act);
    G4double sinh_act = std::sinh(act);


    G4double facteur_r = 1. / aa * (cosh_act - 1.0 +  mag_vpar / c_light * sinh_act);

    G4double facteur_v = c_light * sinh_act + (cosh_act - 1.0) * mag_vpar;

    // Store the resulting position and momentum
    // + energy and time

    yParabola[0] =  yIn[0] + delta_time * vperp.x() + facteur_r * E_hat.x();
    yParabola[1] =  yIn[1] + delta_time * vperp.y() + facteur_r * E_hat.y();
    yParabola[2] =  yIn[2] + delta_time * vperp.z() + facteur_r * E_hat.z();


    G4ThreeVector posi_ini = G4ThreeVector(yIn[0], yIn[1], yIn[2]);
    G4ThreeVector posi_fin = G4ThreeVector(yParabola[0], yParabola[1], yParabola[2]);

    G4double EnergyGain =  particleChargeSign * eplus * Efld.dot(posi_ini - posi_fin); //
    //
    // sign
    //
    // inverted
    //
    // from
    //
    // PENELOPE
    //
    // formula
    yParabola[6] = yIn[6]  + EnergyGain;
    yParabola[7] = yIn[7] + delta_time;

    G4double gamma_end             = 1. + yParabola[6] / electron_mass_c2;
    G4ThreeVector velocity_inc_end = facteur_v * E_hat;
    G4ThreeVector momentum_inc_end = gamma_end * particle_mass * velocity_inc_end;

    yParabola[3] = yIn[3] + momentum_inc_end.x();
    yParabola[4] = yIn[4] + momentum_inc_end.y();
    yParabola[5] = yIn[5] + momentum_inc_end.z();


    //        G4cout << "!!!!!!!!!!!!!!!! CHARGE : "  << yIn[7] << " " << h << " "
    // << 75/1000000 << " " << EnergyGain << G4endl;
    //
    //        if (delta_time<0.){
    //                G4Exception("delta_time negative",
    //               " ", FatalException,
    //               " " );
    //        }
}

inline void G4ExactUniEFStepper::ElecFieldEvaluate(const G4double y[],
        G4ThreeVector &Efield)
{
    G4double B[6];

    GetEquationOfMotion()->GetFieldValue(y, B);

    Efield = G4ThreeVector(B[3], B[4], B[5]);
}

G4int G4ExactUniEFStepper::IntegratorOrder() const
{
    return 1;
}
