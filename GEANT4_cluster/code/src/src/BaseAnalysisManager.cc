//

// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

#include "BaseAnalysisManager.hh"
#include <fstream>
#include <iomanip>
#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4Track.hh"
#include "G4ios.hh"
#include "G4SteppingManager.hh"
#include "G4ThreeVector.hh"
#include "G4PhysicalConstants.hh"

#include <boost/filesystem.hpp>



// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
BaseAnalysisManager *BaseAnalysisManager::instance = 0;

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
BaseAnalysisManager::BaseAnalysisManager()
{
    G4String directoryName = "./output/";

    G4double EFIELD2 = Settings::EFIELD / (volt / m);

    asciiFileName1 = directoryName + std::to_string(Settings::Rand_seed) + "_GEANT4" + Settings::PHYSLIST
                     + "_" + std::to_string(int(EFIELD2))
                     + "_" + std::to_string(int(Settings::ENER_THRES)) + ".txt";


    // creates folder if it does not exist
    if (!(boost::filesystem::exists(directoryName)))
        {
            boost::filesystem::create_directories(directoryName);
        }


    std::ofstream asciiFile1(asciiFileName1, std::ios::trunc); // cleans file

    RECORDED_OUTPUT_STRINGS.clear();



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    ////  recording distances and times
    RECORD_DISTANCES = { 4.,    7.,  10.,   14.,   17.,   21.,   24.,   27.,   31.,  33.,  36.,
                         38.,  41.,  44.,   48.,   53.,   57.,   62.,   70.,   74.,  77.,  83.,
                         94., 108., 124.,  128.,  139.,  160.,  192.,  224.,  256.,  288.
                       }; // meters


    RECORD_TIMES = { 14.,    26.,   39.,   51.,   64.,   78.,   90.,  102.,   116.,  124.,  135.,
                     144.,  153.,  164.,  179.,  199.,  215.,  233.,  262.,   277.,  290.,  312.,
                     351.,  406.,  464.,  479.,  522.,  599.,  719.,  838.,   958., 1078.
                   }; // nanoseconds
    ////

    if (RECORD_TIMES.size() != RECORD_DISTANCES.size())
        {
            G4cout << "ERROR : record times list is not the same size as the record distances list." << G4endl ;
            std::abort();
        }

    //// Applying units
    ///
    for (unsigned int ii = 0; ii < RECORD_TIMES.size(); ii++)
        {
            RECORD_TIMES[ii] = RECORD_TIMES[ii] * nanosecond;
        }

    for (unsigned int ii = 0; ii < RECORD_DISTANCES.size(); ii++)
        {
            RECORD_DISTANCES[ii] = RECORD_DISTANCES[ii] * meter;
        }

}


// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....


G4double BaseAnalysisManager::Min_ener_output() const
{
    return min_ener_output;
}

std::array<G4double, 32> BaseAnalysisManager::get_RECORD_DISTANCES() const
{
    return RECORD_DISTANCES;
}

std::array<G4double, 32> BaseAnalysisManager::get_RECORD_TIMES() const
{
    return RECORD_TIMES;
}


// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BaseAnalysisManager::~BaseAnalysisManager()
{ }

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BaseAnalysisManager *BaseAnalysisManager::getInstance()
{
    if (instance == 0) instance = new BaseAnalysisManager;

    return instance;
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BaseAnalysisManager::output_particle(const int &id_output,
        const double &x,
        const double &y,
        const double &z,
        const double &px,
        const double &py,
        const double &pz,
        const double &time,
        const double &energy)
{
    std::stringstream buffer;
    buffer << std::scientific << std::setprecision(5); // scientific notation with 5 significant digits

    buffer << Settings::NB_LAUNCHED << " "
           << std::to_string(id_output) << " "
           << x << " "
           << y << " "
           << z  << " "
           << px << " "
           << py << " "
           << pz  << " "
           << time  << " "
           << energy << '\n';

    RECORDED_OUTPUT_STRINGS.push_back(buffer.str());

    // will be written to file at end of event
}


// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
void BaseAnalysisManager::write_output_end_of_Event()
{

    if (RECORDED_OUTPUT_STRINGS.size() > 0)
        {
            std::ofstream asciiFile1;
            asciiFile1.open(asciiFileName1, std::ios::app);

            if (asciiFile1.is_open())
                {
                    for (auto & RECORDED_OUTPUT : RECORDED_OUTPUT_STRINGS)
                        {
                            asciiFile1 << RECORDED_OUTPUT;
                        }

                    asciiFile1.close();
                    RECORDED_OUTPUT_STRINGS.clear();
                }
        }
}

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
void BaseAnalysisManager::write_output_endOfRun()
{

    if (RECORDED_OUTPUT_STRINGS.size() > 0)
        {
            std::ofstream asciiFile1;
            asciiFile1.open(asciiFileName1, std::ios::app);

            if (asciiFile1.is_open())
                {
                    for (auto & RECORDED_OUTPUT : RECORDED_OUTPUT_STRINGS)
                        {
                            asciiFile1 << RECORDED_OUTPUT;
                        }

                    asciiFile1.close();
                    RECORDED_OUTPUT_STRINGS.clear();
                }
        }
}
