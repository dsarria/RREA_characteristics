#include "G4UnitsTable.hh"
#include "Settings.hh"
#include "G4SystemOfUnits.hh"

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
namespace Settings
{
    // All these variables are put here to be shared amounts source files
    // (not very c++ standard but easier to implement)

    G4double ENER_THRES = 990.; // eV
    G4String PHYSLIST = "O1";

    int NB_LAUNCHED = 0; // initialization
    int Rand_seed = 779;
    double MIN_STEP = 1.; // um
    double PRIMARY_ENERGY = 0.1; // MeV
    double LOG10_EPSTOL = -5.;
    double EFIELD = 8.e5; // V/m
    int TYPE = -1;              // chosen value
    double TIME_LIMIT = 522.; //nanosecond
    const double EF_SIGN = -1.;
    G4double DR_OVER_R = 0.0005;
    
    bool USE_MAX_STEP = true;
    G4double STEP_MAX = 1.*mm;
    
    bool REMOVE_BELOW_10KEV = true; // performance improvement without getting "bad" results above 10 keV
    
    bool USE_STACKING_ACTION = false; // use of stacking action to mimic time oriented simulation
    // usefull to check maximum (limit) times, or multiplciation factors, but poor performances compare to default behaviour
    
    
    G4double VARIABLE_TIME_LIM = 10e50; // large value just for initialization
}
