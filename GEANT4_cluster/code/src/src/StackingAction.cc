//
// ********************************************************************
#include "StackingAction.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4Track.hh"
#include "G4TrackStatus.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"

#include "G4SystemOfUnits.hh"
#include "G4ios.hh"

// Added to mimic time dependent simulation, and to count the number of energetic electron every 5 microseconds
// and turn OFF the electric field if there is more than 100000 (-> RREA)

// can be a bad idea to set it on, because it can use a lot of memory compared to default G4 behaviour

BaseStackingAction::BaseStackingAction()
{

    TIME_STEP = DELTA_T ;

}

BaseStackingAction::~BaseStackingAction()
{  }

void BaseStackingAction::PrepareNewEvent()
{
    LIST_ENERGETIC_PART_IDS.clear();
    //    LIST_ENERGIES.clear();
    VARIABLE_TIME_LIMIT = TIME_STEP;
    EVENT_NB++;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

G4ClassificationOfNewTrack BaseStackingAction::ClassifyNewTrack(const G4Track *aTrack)
{

    // Warning : dot not use "aTrack->GetStep()->GetPreStepPoint()", this is not the stepping action, step may not exist

    const G4int ID = aTrack->GetTrackID();

    // if energy > 10 keV and if the ID is not already saved
    if (does_not_contain(ID, LIST_ENERGETIC_PART_IDS))
        {
            if (aTrack->GetKineticEnergy() > ENER_THRES)
                {
                    LIST_ENERGETIC_PART_IDS.push_back(ID); // save the ID
                    //            LIST_ENERGIES.push_back(ener);
                }
        }

    if (aTrack->GetGlobalTime() > VARIABLE_TIME_LIMIT)
        {
            return fWaiting;
        }

    // default classification
    return fUrgent;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BaseStackingAction::print_status()
{
    G4cout << "current max time : " << VARIABLE_TIME_LIMIT / nanosecond << " nanosecond(s)" << G4endl;
    G4cout << ">10 keV electron count : " << LIST_ENERGETIC_PART_IDS.size() << G4endl;
    G4cout << "Event nb : " << EVENT_NB << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BaseStackingAction::NewStage() // called when the "Urgent stack" is empty
// and the particles in the "waiting stack" are transfered to the "Urgent stack"
{

    VARIABLE_TIME_LIMIT += TIME_STEP;

    Settings::VARIABLE_TIME_LIM = VARIABLE_TIME_LIMIT;

    print_status();

    LIST_ENERGETIC_PART_IDS.clear();

    return;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

bool BaseStackingAction::does_not_contain(const G4int ID, const std::vector<G4int> &LIST_IDS)
{
    if (LIST_IDS.empty()) return true;

    if (std::find(LIST_IDS.begin(), LIST_IDS.end(), ID) != LIST_IDS.end()) return false;
    else return true;
}

