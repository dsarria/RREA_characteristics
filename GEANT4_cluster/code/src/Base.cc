//

// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************


#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "Randomize.hh"

#ifdef G4VIS_USE
# include "G4VisExecutive.hh"
#endif // ifdef G4VIS_USE

#ifdef G4UI_USE
# include "G4UIExecutive.hh"
#endif // ifdef G4UI_USE

#include "LBE.hh"

// #include "G4EmPenelopePhysics.hh"
#include "FTFP_BERT_HP.hh"
#include <G4PhysListFactory.hh>

#include "BaseDetectorConstruction.hh"
#include "BasePrimaryGeneratorAction.hh"
#include "BaseRunAction.hh"
#include "BaseEventAction.hh"
#include "BaseTrackingAction.hh"
#include "BaseSteppingAction.hh"

#include "BasePhysicsList.hh"
#include "G4NistManager.hh"
#include <time.h>
#include <iostream>
#include <fstream>


#include <time.h>
#include <sys/time.h>

#include "Settings.hh"
#include "StackingAction.hh"



double get_wall_time()
{
    struct timeval tv;

    gettimeofday(&tv, NULL);
    return tv.tv_sec + (tv.tv_usec / 1000000.0);
}

int main(int argc, char **argv)
{

    //    BaseAnalysisManager *analysis = BaseAnalysisManager::getInstance();

    G4String NUMBER_INI = "0";

    double T0 = get_wall_time();


    // reading input arguments
    G4cout << argc << G4endl;

    if (argc >= 4)
        {
            std::cout << "Argument list" << std::endl;

            for (int i = 0; i < argc; i++)
                {
                    std::cout << argv[i] << std::endl;
                }

            Settings::PHYSLIST       = argv[1];
            NUMBER_INI      = argv[2];
            Settings::EFIELD         = std::stod(argv[3]); // V/m
            Settings::LOG10_EPSTOL   = std::stod(argv[4]);
            Settings::PRIMARY_ENERGY = std::stod(argv[5]); // MeV
            Settings::TIME_LIMIT     = std::stod(argv[6]);
            Settings::Rand_seed      = std::stoi(argv[7]);
            Settings::ENER_THRES     = std::stod(argv[8]);
            Settings::STEP_MAX       = std::stod(argv[9]) * mm;
            Settings::DR_OVER_R      = std::stod(argv[10]);
        }
    else
        {
            // default values
            Settings::PHYSLIST       = "O1";
            NUMBER_INI      = "50";
            Settings::EFIELD         = 8.e5; // V/m
            Settings::LOG10_EPSTOL   = -5;
            Settings::PRIMARY_ENERGY = 0.1;   // MeV
            Settings::Rand_seed      = 779;
            Settings::TIME_LIMIT     = 522.;
            Settings::ENER_THRES     = 990.; // eV
            Settings::STEP_MAX = 1 * mm;
            Settings::DR_OVER_R = 0.1;
        }


    //////////// applying units to inputs
    Settings::EFIELD = Settings::EFIELD * volt / meter;
    Settings::TIME_LIMIT = Settings::TIME_LIMIT * nanosecond;
    ////////////

    //    G4Random::setTheEngine(new CLHEP::MTwistEngine);// choose the Random engine
    
    // set the seed
    G4int seed = Settings::Rand_seed;
    G4Random::setTheSeed(seed);

    // Construct the default run manager
    G4RunManager *runManager = new G4RunManager;

    // set mandatory initialization classes
    BaseDetectorConstruction *detector = new BaseDetectorConstruction();
    runManager->SetUserInitialization(detector);


    //   runManager->SetUserInitialization(new LBE());

    if (Settings::PHYSLIST == "LBE")
        {
            G4PhysListFactory *physListFactory = new G4PhysListFactory();
            G4VUserPhysicsList *physicsList = physListFactory->GetReferencePhysList("LBE");

            G4EmParameters *param = G4EmParameters::Instance();

            G4double lowlimit = Settings::ENER_THRES * CLHEP::eV;
            G4ProductionCutsTable *aPCTable = G4ProductionCutsTable::GetProductionCutsTable();
            aPCTable->SetEnergyRange(lowlimit, 1000. * CLHEP::MeV);
            param->SetMaxEnergy(1000.*MeV);

            runManager->SetUserInitialization(physicsList);
        }
    else
        {
            runManager->SetUserInitialization(new BasePhysicsList);
        }

    //

    runManager->SetUserAction(new BasePrimaryGeneratorAction());

#ifdef G4VIS_USE

    // visualization manager
    G4VisManager *visManager = new G4VisExecutive;
    visManager->Initialize();
#endif // ifdef G4VIS_USE


    // set user action classes
    BaseRunAction *RunAct = new BaseRunAction();
    runManager->SetUserAction(RunAct);
    runManager->SetUserAction(new BaseEventAction());
    
    if (Settings::USE_STACKING_ACTION) {
      G4UserStackingAction *stackingAction = new BaseStackingAction();
      runManager->SetUserAction(stackingAction);
    }


    // SteppingAction
    runManager->SetUserAction(new BaseSteppingAction(detector));


    // Initialize G4 kernel
    runManager->Initialize();

    // get the pointer to the User Interface manager
    G4UImanager *UImanager = G4UImanager::GetUIpointer();

    UImanager->ApplyCommand("/run/beamOn " + NUMBER_INI);


    // #ifdef G4UI_USE
    //       G4UIExecutive* ui = new G4UIExecutive(argc, argv);
    // #ifdef G4VIS_USE
    //       UImanager->ApplyCommand("/control/execute vis.mac");
    // #endif
    //       ui->SessionStart();
    //       delete ui;
    // #endif


    // job termination
#ifdef G4VIS_USE
    delete visManager;
#endif // ifdef G4VIS_USE
    delete runManager;


    double T1 = get_wall_time();

    G4cout << G4endl << "Time taken : " << T1 - T0 << G4endl;

    return 0;
}
