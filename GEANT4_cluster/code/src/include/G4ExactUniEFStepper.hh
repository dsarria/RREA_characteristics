//

// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: G4ExactUniEFStepper.hh 66356 2012-12-18 09:02:32Z gcosmo $
//
//
// class G4ExactUniEFStepper
//       -------------------
// Class description:
//
// Concrete class for particle motion in constant electric field.

// History:
// - 20.Dec.16  D. Sarria   Creation of new concrete class
// --------------------------------------------------------------------


// NOT WORKING YET !!!!!

#pragma once

#include "G4Types.hh"
#include "G4ThreeVector.hh"
#include "G4SystemOfUnits.hh"
#include "G4MagIntegratorStepper.hh"
#include "G4MagHelicalStepper.hh"
#include "G4Mag_EqRhs.hh"
#include "G4EqMagElectricField.hh"
#include <CLHEP/Units/PhysicalConstants.h>


class G4ExactUniEFStepper: public G4MagIntegratorStepper
{
    public:

        // with description

        G4ExactUniEFStepper(G4EqMagElectricField *EqMagElectricField);
        ~G4ExactUniEFStepper();

        void Stepper(const G4double y[],
                     const G4double dydx[],
                     G4double       h,
                     G4double       yout[],
                     G4double       yerr[]);

        // Step 'integration' for step size 'h'
        // Provides parabola starting at y[0 to 8]
        // Outputs yout[] and ZERO estimated error yerr[]=0.

        void DumbStepper(const G4double y[],
                         G4ThreeVector  Bfld,
                         G4double       h,
                         G4double       yout[]);

        // Performs a 'dump' Step without error calculation.

        G4double DistChord() const;

        // Estimate maximum distance of curved solution and chord ...

        virtual G4int IntegratorOrder() const;

    private:

        G4ExactUniEFStepper(const G4ExactUniEFStepper &);
        G4ExactUniEFStepper &operator = (const G4ExactUniEFStepper &);

        // Private copy constructor and assignment operator.


        // STATE
        G4ThreeVector fInitialPoint, fMidPoint, fFinalPoint;

        // Data stored in order to find the chord

    protected:

        void AdvanceParabola(const G4double      yIn[],
                             const G4ThreeVector Bfld,
                             const G4double      h,
                             G4double            yParabola[]); // output

        inline void ElecFieldEvaluate(const G4double y[],
                                      G4ThreeVector &Efield);

        // Evaluate the E-field at a certain point.
};


