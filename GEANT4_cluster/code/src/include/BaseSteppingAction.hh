//

// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#pragma once

#include "G4UserSteppingAction.hh"
#include "G4VPVParameterisation.hh"
#include "G4PVParameterised.hh"
#include "BaseAnalysisManager.hh"
#include "Settings.hh"

class BaseDetectorConstruction;
class BaseAnalysisManager;

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

class BaseSteppingAction: public G4UserSteppingAction
{
    public:

        BaseSteppingAction(BaseDetectorConstruction *);
        ~BaseSteppingAction();

        void UserSteppingAction(const G4Step *);

    private:

        BaseDetectorConstruction *Detector;

        G4double px     = 0;
        G4double py     = 0;
        G4double pz     = 0;

        G4ThreeVector calc_momentum_SI(const G4int      PDG_nb,
                                       const G4ThreeVector &mom_dir,
                                       const G4double      &average_energy);

        G4int get_id_output(const G4int PDG_nb);

        BaseAnalysisManager *analysis = BaseAnalysisManager::getInstance();

        std::array<G4double, 32> RECORD_DISTANCES_copy = analysis->get_RECORD_DISTANCES();
        std::array<G4double, 32> RECORD_TIMES_copy = analysis->get_RECORD_TIMES();

        G4double MinEner_copy = analysis->Min_ener_output();
        G4double interp_property(const G4double ener_pre, const G4double ener_post, const G4double time_pre, const G4double time_post, const G4double record);

        G4double E_field_MeV_mm = Settings::EFIELD; // energy gain per in MeV per mm (base units of GEANT4), it is se to the right unit in the Base.cc file

};


