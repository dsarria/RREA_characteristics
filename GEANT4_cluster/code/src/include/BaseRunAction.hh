#pragma once

#include "G4UserRunAction.hh"
#include "globals.hh"
#include <iostream>
#include "BaseAnalysisManager.hh"

class G4Run;

class BaseRunAction: public G4UserRunAction
{
    public:

        BaseRunAction();
        ~BaseRunAction();

        void   BeginOfRunAction(const G4Run *);
        void   EndOfRunAction(const G4Run *);

        double get_wall_time(); // getting wall time in second

        void   SetRndmFreq(G4int val)
        {
            saveRndm = val;
        }

        G4int GetRndmFreq()
        {
            return saveRndm;
        }

    private:

        G4int saveRndm;

        BaseAnalysisManager *analysis = BaseAnalysisManager::getInstance();
};


