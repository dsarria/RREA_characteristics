#pragma once


#include "G4SystemOfUnits.hh"
#include "G4VUserPhysicsList.hh"
#include "Settings.hh"

class G4VPhysicsConstructor;

class BasePhysicsList: public G4VUserPhysicsList
{
    public:

        BasePhysicsList();
        ~BasePhysicsList();

        void SetCuts();
        void ConstructParticle();
        void ConstructProcess();

    private:

        G4double cutForGamma;
        G4double cutForElectron;
        G4double cutForPositron;

        G4VPhysicsConstructor *emPhysicsList;
        G4VPhysicsConstructor *radDecPhysicsList;

        void AddStepMax(G4double stepMax_elec, G4double stepMax_phot);
};

