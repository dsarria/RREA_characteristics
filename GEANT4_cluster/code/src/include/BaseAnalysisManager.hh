//

// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************


#pragma once

#include "globals.hh"
#include <vector>
#include "G4ThreeVector.hh"
#include "G4Track.hh"
#include "g4csv.hh"
#include "Settings.hh"

// #include "g4root.hh"
// #include "g4xml.hh"

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

class BaseAnalysisManager
{
    private:


    public:

        ~BaseAnalysisManager();

        static BaseAnalysisManager *getInstance();

        void output_particle(const int    &id_output,
                             const double &x,
                             const double &y,
                             const double &z,
                             const double &px,
                             const double &py,
                             const double &pz,
                             const double &time,
                             const double &energy);

        void write_output_end_of_Event();
        void write_output_endOfRun();

        std::array<G4double, 32> get_RECORD_TIMES() const;

        std::array<G4double, 32> get_RECORD_DISTANCES() const;

        G4double Min_ener_output() const;

    private:

        static BaseAnalysisManager *instance;

        BaseAnalysisManager();

        G4String asciiFileName1;

        std::vector<G4String> RECORDED_OUTPUT_STRINGS;

        const unsigned int output_buffer_size = 1;


        std::array<G4double, 32> RECORD_TIMES;
        std::array<G4double, 32> RECORD_DISTANCES;
        const unsigned int nb_record = 32;

        const G4double min_ener_output = 10. ; // keV


};

