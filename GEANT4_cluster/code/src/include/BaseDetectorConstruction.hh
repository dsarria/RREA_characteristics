//

// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************


// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#pragma once

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"

#include "G4Tubs.hh"
#include "G4Mag_UsualEqRhs.hh"
#include "G4FieldManager.hh"
#include "G4UniformElectricField.hh"
#include "G4TransportationManager.hh"
#include "G4EqMagElectricField.hh"
#include "G4ElectroMagneticField.hh"

#include "Settings.hh"

class G4Box;
class G4Trd;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class G4UniformMagField;

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

class BaseDetectorConstruction: public G4VUserDetectorConstruction
{
    public:

        BaseDetectorConstruction();
        ~BaseDetectorConstruction();

    public:

        G4VPhysicalVolume *Construct();

    public:

        static BaseDetectorConstruction *getInstance();

        void                             PrintDetectorParameters();

        G4double                         GetWorldSizeXY()
        {
            return WorldSizeXY;
        }

        G4double GetWorldSizeZ()
        {
            return WorldSizeZ;
        }



        G4Material *GetWorldMaterial()
        {
            return WorldMaterial;
        }

        const G4VPhysicalVolume *GetWorld()
        {
            return physiWorld;
        }

    private:

        G4double WorldSizeXY;
        G4double WorldSizeZ;


        G4VPhysicalVolume *physiWorld;
        G4LogicalVolume   *logicWorld;
        G4Tubs *solidWorld;



        G4Material *WorldMaterial;
        G4Material *MeasureMaterial;



        G4Material   *Air;



    private:

        static BaseDetectorConstruction *instance;
        void               DefineMaterials();
        G4VPhysicalVolume *ConstructWorld();
        //  void               ReadInputDataFile();
        void create_Efield();

        G4FieldManager *globalfieldMgr;
        //        G4FieldManager *localfieldMgr;

};


