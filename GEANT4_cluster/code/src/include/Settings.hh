#pragma once

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
namespace Settings
{
    // All these variables are put here to be shared amounts source files
    // (not very c++ but easier to implement)

    extern G4double ENER_THRES; // eV
    extern G4String PHYSLIST;

    extern int NB_LAUNCHED; // initialization
    extern int Rand_seed;
    extern double MIN_STEP;   // um
    extern double PRIMARY_ENERGY;  // MeV
    extern double LOG10_EPSTOL;
    extern double EFIELD; // V/m
    extern int TYPE;                // chosen value
    extern double TIME_LIMIT;
    extern const double EF_SIGN;
    extern G4double DR_OVER_R;
    
    extern bool USE_MAX_STEP;
    extern G4double STEP_MAX;
    
    extern bool REMOVE_BELOW_10KEV;
    extern bool USE_STACKING_ACTION; // use of stacking action to mimic time oriented simulation
    
    extern G4double VARIABLE_TIME_LIM;
}
